import request from '@/utils/request'

const api_name = '/admin/hosp/hospitalSet'
/**
 * 医院设置表
 */
export default {

  // @ApiOperation("8 医院设置锁定和解锁-更新status")
  lockHospSet(id, status) {
    return request({
      url: `${api_name}/lockHospitalSet/${id}/${status}`,
      method: 'put'
    })
  },
  // @ApiOperation("7 删除医院设置-批量删除")
  batchRemoveHospitalSet(idList) {
    return request({
      url: `${api_name}/batchRemove`,
      method: 'delete',
      data: idList
    })
  },
  // @ApiOperation("6-1 根据ID查询医院设置")
  getHospSetById(id) {
    return request({
      url: `${api_name}/getHospSet/${id}`,
      method: 'get'
    })
  },
  // @ApiOperation("6-2 根据ID修改医院设置")
  updateById(hospitalSet) {
    return request({
      url: `${api_name}/updateHospSet`,
      method: 'post',
      data: hospitalSet
    })
  },

  // @ApiOperation("5.添加医院设置")
  saveHospSet(hospitalSet) {
    return request({
      url: `${api_name}/saveHospSet`,
      method: 'post',
      data: hospitalSet
    })
  },

  // @ApiOperation("4.删除医院设置信息-id")
  removeById(id) {
    return request({
      url: `${api_name}/remove/${id}`,
      method: 'delete'
    })
  },

  // @ApiOperation("3.查询医院设置列表-条件查询带分页")
  getPageList(currentPage, limit, hospitalSetQueryVo) {
    return request({
      url: `${api_name}/findPageQuery/${currentPage}/${limit}`,
      method: 'post',
      // JSON格式数据,使用data:参数
      // 非JSON格式数据,使用params:参数
      data: hospitalSetQueryVo
    })
  }
}