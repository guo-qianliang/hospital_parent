/**
 * 定义接口的路径、参数等信息
 */
import request from '@/utils/request'
/**
 * 登录
 */
export function login(data) {
  return request({
    url: '/admin/hosp/user/login',
    method: 'post',
    data
  })
}
/**
 * 获取信息
 */
export function getInfo(token) {
  return request({
    url: '/admin/hosp/user/info',
    method: 'get',
    params: { token }
  })
}
/**
 * 注销
 */
// export function logout() {
//   return request({
//     url: '/vue-admin-hospital/user/logout',
//     method: 'post'
//   })
// }
