package com.alibaba.yygh.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * description: FileServiceImpl
 * @author: gql
 * @date: 2021/11
 */
public interface FileService {

    /**
     * 1.文件上传至阿里云
     */
    String upload(MultipartFile file);
}
