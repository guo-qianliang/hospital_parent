package com.alibaba.yygh.user.service.impl;

import com.alibaba.yygh.common.exception.YyghException;
import com.alibaba.yygh.common.utils.JwtHelper;
import com.alibaba.yygh.enums.AuthStatusEnum;
import com.alibaba.yygh.model.user.Patient;
import com.alibaba.yygh.model.user.UserInfo;
import com.alibaba.yygh.user.mapper.UserInfoMapper;
import com.alibaba.yygh.user.service.PatientService;
import com.alibaba.yygh.user.service.UserInfoService;
import com.alibaba.yygh.vo.user.LoginVo;
import com.alibaba.yygh.vo.user.UserAuthVo;
import com.alibaba.yygh.vo.user.UserInfoQueryVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author gql
 * @since 2021-11-12
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 1.会员登录
     * @param loginVo
     * @return
     */
    @Override
    public Map<String, Object> loginUser(LoginVo loginVo) {
        // 1.1 获取用户手机号、用户输入的验证码 并校验
        String phone = loginVo.getPhone();
        String code = loginVo.getCode();
        if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(code)) {
            throw new YyghException(20001, "手机号或验证码为空");
        }

        // 1.2 根据手机号获取redis存储的验证码
        String redisCode = this.redisTemplate.opsForValue().get(phone);
        // 1.3 将用户输入的验证码和redis中的验证码比对
        if (!code.equals(redisCode)) {
            throw new YyghException(20001, "验证码校验失败");
        }

        // 判断openid值是否为空,如果为空就手机验证码登录,如果不为空微信绑定手机号

        // 判断openid
        String openid = loginVo.getOpenid();
        if (StringUtils.isEmpty(openid)) {// 手机验证码登录
            // 2. 如果是第一次登录，把手机号添加到数据库进行注册
            QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("phone", phone);
            UserInfo userInfo = baseMapper.selectOne(queryWrapper);
            if (null == userInfo) {
                userInfo = new UserInfo();
                userInfo.setName("");
                userInfo.setPhone(phone);
                userInfo.setStatus(1);
                this.save(userInfo);
            }

            // 3. 判断用户是否被禁用
            if (userInfo.getStatus() == 0) {
                throw new YyghException(20001, "用户已经被禁用");
            }

            // 4. 封装后返回
            Map<String, Object> map = get(userInfo);
            return map;
        } else {// 绑定手机号
            // 判断表中是否存在相同的手机号的数据
            QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
            wrapper.eq("phone", phone);
            UserInfo userInfoPhone = baseMapper.selectOne(wrapper);

            if (userInfoPhone == null) {// 表中不存在相同绑定手机号
                // 根据openid获取微信信息
                UserInfo userInfoWx = this.getUserInfoByOpenId(openid);
                if (userInfoWx == null) {
                    throw new YyghException(20001, "微信信息不存在");
                }
                // 设置绑定手机号
                userInfoWx.setPhone(phone);
                // 更新
                baseMapper.updateById(userInfoWx);
                // 封装后返回
                Map<String, Object> map = get(userInfoWx);
                return map;
            } else {// 表中存在相同绑定手机号,将手机号登录过的数据,更新到微信中
                // 根据openid获取微信数据
                UserInfo userInfoWx = this.getUserInfoByOpenId(openid);
                // 更新手机号
                userInfoWx.setPhone(userInfoPhone.getPhone());
                // 更新用户真实姓名
                userInfoWx.setName(userInfoPhone.getName());
                // 更新其他内容
                userInfoWx.setCertificatesType(userInfoPhone.getCertificatesType());
                userInfoWx.setCertificatesNo(userInfoPhone.getCertificatesNo());
                userInfoWx.setCertificatesUrl(userInfoPhone.getCertificatesUrl());
                // 删除手机号数据
                baseMapper.delete(new QueryWrapper<UserInfo>().eq("phone", phone));
                // 更新
                baseMapper.updateById(userInfoWx);
                // 封装后返回
                Map<String, Object> map = get(userInfoWx);
                return map;
            }
        }
    }

    /**
     * 抽取出来的get方法
     * @param userInfo
     * @return
     */
    private Map<String, Object> get(UserInfo userInfo) {
        // 返回页面显示名称
        Map<String, Object> map = new HashMap<>();
        String name = userInfo.getName();
        // 如果姓名是空就显示昵称
        if (StringUtils.isEmpty(name)) {
            name = userInfo.getNickName();
        }
        // 如果昵称也为空就显示手机号
        if (StringUtils.isEmpty(name)) {
            name = userInfo.getPhone();
        }
        map.put("name", name);
        /**
         * 根据userId和userName生成token,为单点登录做准备
         */
        String token = JwtHelper.createToken(userInfo.getId(), name);
        map.put("token", token);
        return map;
    }

    /**
     * 2.根据openid查询是否存在微信信息
     * @param openid
     * @return
     */
    @Override
    public UserInfo getUserInfoByOpenId(String openid) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("openid", openid);
        UserInfo userInfo = baseMapper.selectOne(wrapper);
        return userInfo;
    }

    /**
     * 3. 用户认证接口
     * @param userId 用户id
     * @param userAuthVo 认证数据vo对象
     */
    @Override
    public void userAuth(Long userId, UserAuthVo userAuthVo) {
        // 根据用户id查询用户信息
        UserInfo userInfo = baseMapper.selectById(userId);
        // 设置认证信息
        userInfo.setName(userAuthVo.getName());// 认证人姓名
        userInfo.setCertificatesType(userAuthVo.getCertificatesType());// 其他认证信息
        userInfo.setCertificatesNo(userAuthVo.getCertificatesNo());
        userInfo.setCertificatesUrl(userAuthVo.getCertificatesUrl());
        userInfo.setAuthStatus(AuthStatusEnum.AUTH_RUN.getStatus());
        // 进行信息更新
        baseMapper.updateById(userInfo);
    }

    /**
     * 用户列表-条件查询带分页
     * @param pageParam
     * @param userInfoQueryVo
     * @return
     */
    @Override
    public IPage<UserInfo> selectPage(Page<UserInfo> pageParam, UserInfoQueryVo userInfoQueryVo) {
        // 1.获取条件值
        String name = userInfoQueryVo.getKeyword(); //用户名称
        Integer status = userInfoQueryVo.getStatus();//用户状态
        Integer authStatus = userInfoQueryVo.getAuthStatus(); //认证状态
        String createTimeBegin = userInfoQueryVo.getCreateTimeBegin(); //开始时间
        String createTimeEnd = userInfoQueryVo.getCreateTimeEnd(); //结束时间
        // 2.判断条件值是否为空,不为空就封装条件
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(name)) {
            wrapper.like("name", name);
        }
        if (!StringUtils.isEmpty(status)) {
            wrapper.eq("status", status);
        }
        if (!StringUtils.isEmpty(authStatus)) {
            wrapper.eq("auth_status", authStatus);
        }
        if (!StringUtils.isEmpty(createTimeBegin)) {
            wrapper.ge("create_time", createTimeBegin);
        }
        if (!StringUtils.isEmpty(createTimeEnd)) {
            wrapper.le("create_time", createTimeEnd);
        }
        // 3.调用mapper的方法
        IPage<UserInfo> pages = baseMapper.selectPage(pageParam, wrapper);
        //编号变成对应值封装
        pages.getRecords().stream().forEach(item -> {
            this.packageUserInfo(item);
        });
        return pages;
    }

    /**
     * 锁定
     * @param userId 用户id
     * @param status 状态 0:锁定,1:正常
     */
    @Override
    public void lock(Long userId, Integer status) {
        if (status.intValue() == 0 || status.intValue() == 1) {
            UserInfo userInfo = this.getById(userId);
            userInfo.setStatus(status);
            this.updateById(userInfo);
        }
    }


    @Autowired
    private PatientService patientService;
    /**
     * 3.用户详情
     * @param userId
     * @return
     */
    @Override
    public Map<String, Object> showUser(Long userId) {
        // 根据userid查询用户基本信息并封装
        UserInfo userInfo = this.packageUserInfo(baseMapper.selectById(userId));
        // 根据userid查询用户下面的就诊人列表
        List<Patient> list = this.patientService.findAllPatient(userId);

        // 返回到前端的map集合中
        Map<String,Object> map = new HashMap<>();
        map.put("userInfo",userInfo);
        map.put("patientList",list);
        return map;
    }

    /**
     * 4.认证审批 2通过  -1不通过
     * @param userId
     * @param authStatus
     */
    @Override
    public void approval(Long userId, Integer authStatus) {
        if(authStatus.intValue()==2 || authStatus.intValue()==-1) {
            UserInfo userInfo = baseMapper.selectById(userId);
            userInfo.setAuthStatus(authStatus);
            baseMapper.updateById(userInfo);
        }
    }


    /**
     * 编号变成对应值封装
     * @param userInfo
     * @return
     */
    private UserInfo packageUserInfo(UserInfo userInfo) {
        //处理认证状态编码
        userInfo.getParam().put("authStatusString", AuthStatusEnum.getStatusNameByStatus(userInfo.getAuthStatus()));
        //处理用户状态 0  1
        String statusString = userInfo.getStatus().intValue() == 0 ? "锁定" : "正常";
        userInfo.getParam().put("statusString", statusString);
        return userInfo;
    }
}
