package com.alibaba.yygh.user.service.impl;

import com.alibaba.yygh.client.DictFeignClient;
import com.alibaba.yygh.enums.DictEnum;
import com.alibaba.yygh.model.user.Patient;
import com.alibaba.yygh.user.mapper.PatientMapper;
import com.alibaba.yygh.user.service.PatientService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 就诊人表 服务实现类
 * </p>
 *
 * @author gql
 * @since 2021-11-14
 */
@Service
public class PatientServiceImpl extends ServiceImpl<PatientMapper, Patient> implements PatientService {

    /**
     * DictFeignClient中封装了根据dictCode和value获取数据字典名称的返回
     */
    @Autowired
    private DictFeignClient dictFeignClient;

    /**
     * 1.获取就诊人列表
     * @param userId 用户id
     * @return 就诊人列表
     */
    @Override
    public List<Patient> findAllPatient(Long userId) {
        QueryWrapper<Patient> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<Patient> patientList = baseMapper.selectList(wrapper);
        // 遍历封装
        patientList.stream().forEach(item -> {
            this.packagePatient(item);
        });
        return patientList;
    }

    /**
     * 3.根据id获取就诊人信息
     * @param id 就诊人id
     * @return
     */
    @Override
    public Patient getPatientId(Long id) {
        // 获取就诊人信息后封装
        Patient patient = this.packagePatient(baseMapper.selectById(id));
        return patient;
    }

    /**
     * 遍历封装-获取编号对应的名称
     * @param item 就诊人信息
     */
    private Patient packagePatient(Patient item) {
        // 根据编号获得省市区的名字
        String provinceString = dictFeignClient.getName(item.getProvinceCode());
        String cityString = dictFeignClient.getName(item.getCityCode());
        String districtString = dictFeignClient.getName(item.getDistrictCode());
        // 获得联系人证件类型名称
        String certificatesTypeString =
                dictFeignClient.getName(DictEnum.CERTIFICATES_TYPE.getDictCode(), item.getCertificatesType());

        // 将省市区信息和证件类型信息放入map集合
        item.getParam().put("provinceString", provinceString);
        item.getParam().put("cityString", cityString);
        item.getParam().put("districtString", districtString);
        item.getParam().put("certificatesTypeString", certificatesTypeString);
        // 放入完整地址
        item.getParam().put("fullAddress", provinceString + cityString + districtString + item.getAddress());
        return item;
    }
}
