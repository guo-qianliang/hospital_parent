package com.alibaba.yygh.user.controller;


import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.common.utils.AuthContextHolder;
import com.alibaba.yygh.model.user.UserInfo;
import com.alibaba.yygh.user.service.UserInfoService;
import com.alibaba.yygh.vo.user.LoginVo;
import com.alibaba.yygh.vo.user.UserAuthVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author gql
 * @since 2021-11-12
 */
@RestController
@RequestMapping("/api/user")
public class UserInfoController {
    @Autowired
    private UserInfoService userInfoService;

    @ApiOperation(value = "1.会员登录")
    @PostMapping("login")
    public R login(@RequestBody LoginVo loginVo) {
        Map<String, Object> info = userInfoService.loginUser(loginVo);
        return R.ok().data(info);
    }

    @ApiOperation(value = "2.用户认证接口")
    @PostMapping("auth/userAuth")
    public R userAuth(@RequestBody UserAuthVo userAuthVo, HttpServletRequest request) {
        // 传递两个参数，第一个参数用户id，第二个参数认证数据vo对象
        userInfoService.userAuth(AuthContextHolder.getUserId(request),userAuthVo);
        return R.ok();
    }

    @ApiOperation(value = "3.获取用户id信息接口")
    @GetMapping("auth/getUserInfo")
    public R getUserInfo(HttpServletRequest request) {
        // 根据请求头得到用户id
        Long userId = AuthContextHolder.getUserId(request);
        // 根据用户id得到用户信息
        UserInfo userInfo = userInfoService.getById(userId);
        return R.ok().data("userInfo",userInfo);
    }
}

