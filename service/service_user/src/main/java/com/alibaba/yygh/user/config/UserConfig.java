package com.alibaba.yygh.user.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * description: UserConfig
 * @author: gql
 * @date: 2021/11
 */
@Configuration
@MapperScan("com.alibaba.yygh.user.mapper")
public class UserConfig {
    /**
     * mp的分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

}
