package com.alibaba.yygh.user.service;

import com.alibaba.yygh.model.user.UserInfo;
import com.alibaba.yygh.vo.user.LoginVo;
import com.alibaba.yygh.vo.user.UserAuthVo;
import com.alibaba.yygh.vo.user.UserInfoQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author gql
 * @since 2021-11-12
 */
public interface UserInfoService extends IService<UserInfo> {
    /**
     * 1.会员登录
     * @param loginVo
     * @return
     */
    Map<String, Object> loginUser(LoginVo loginVo);

    /**
     * 2.根据openid查询是否存在微信信息
     * @param openid
     * @return
     */
    UserInfo getUserInfoByOpenId(String openid);

    /**
     * 3. 用户认证接口
     * @param userId 用户id
     * @param userAuthVo 认证数据vo对象
     */
    void userAuth(Long userId, UserAuthVo userAuthVo);

    /**
     * 用户列表-条件查询带分页
     * @param pageParam
     * @param userInfoQueryVo
     * @return
     */
    IPage<UserInfo> selectPage(Page<UserInfo> pageParam, UserInfoQueryVo userInfoQueryVo);

    /**
     * 锁定
     * @param userId
     * @param status
     */
    void lock(Long userId, Integer status);

    /**
     * 3.用户详情
     * @param userId
     * @return
     */
    Map<String, Object> showUser(Long userId);

    /**
     * 4.认证审批 2通过  -1不通过
     * @param userId
     * @param authStatus
     */
    void approval(Long userId, Integer authStatus);
}
