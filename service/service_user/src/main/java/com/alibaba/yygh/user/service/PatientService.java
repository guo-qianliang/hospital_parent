package com.alibaba.yygh.user.service;

import com.alibaba.yygh.model.user.Patient;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 就诊人表 服务类
 * </p>
 *
 * @author gql
 * @since 2021-11-14
 */
public interface PatientService extends IService<Patient> {
    /**
     * 1.获取就诊人列表
     * @param userId 用户id
     * @return
     */
    List<Patient> findAllPatient(Long userId);

    /**
     * 3.根据id获取就诊人信息
     * @param id 就诊人id
     * @return
     */
    Patient getPatientId(Long id);

}
