package com.alibaba.yygh.user.controller;


import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.common.utils.AuthContextHolder;
import com.alibaba.yygh.model.user.Patient;
import com.alibaba.yygh.user.service.PatientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 就诊人表 前端控制器 就诊人管理接口
 * </p>
 *
 * @author gql
 * @since 2021-11-14
 */

@RestController
@RequestMapping("/api/user/patient")
public class PatientController {
    @Autowired
    private PatientService patientService;

    @ApiOperation(value = "1.获取就诊人列表")
    @GetMapping("auth/findAll")
    public R findAll(HttpServletRequest request) {
        // 通过请求头中的token获取当前登录的用户id
        Long userId = AuthContextHolder.getUserId(request);
        // 根据用户id查找就诊人
        List<Patient> list = patientService.findAllPatient(userId);
        return R.ok().data("list", list);
    }

    @ApiOperation(value = "2.添加就诊人")
    @PostMapping("auth/save")
    public R savePatient(@RequestBody Patient patient, HttpServletRequest request) {
        // 通过请求头中的token获取当前登录的用户id
        Long userId = AuthContextHolder.getUserId(request);
        // 将用户id添加到patient
        patient.setUserId(userId);
        patientService.save(patient);
        return R.ok();
    }

    @ApiOperation(value = "3.根据id获取就诊人信息")
    @GetMapping("auth/get/{id}")
    public R getPatient(@PathVariable Long id) {
        Patient patient = patientService.getPatientId(id);
        return R.ok().data("patient", patient);
    }

    @ApiOperation(value = "4.修改就诊人")
    @PostMapping("auth/update")
    public R updatePatient(@RequestBody Patient patient) {
        patientService.updateById(patient);
        return R.ok();
    }

    @ApiOperation(value = "5.删除就诊人")
    @DeleteMapping("auth/remove/{id}")
    public R removePatient(@PathVariable Long id) {
        patientService.removeById(id);
        return R.ok();
    }

    @ApiOperation(value = "生成订单-根据就诊人id获得就诊人信息")
    @GetMapping("inner/get/{id}")
    public Patient getPatientOrder(
            @PathVariable("id") Long id) {
        return patientService.getById(id);
    }

}

