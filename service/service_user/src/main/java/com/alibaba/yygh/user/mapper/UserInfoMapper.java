package com.alibaba.yygh.user.mapper;

import com.alibaba.yygh.model.user.UserInfo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author gql
 * @since 2021-11-12
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
