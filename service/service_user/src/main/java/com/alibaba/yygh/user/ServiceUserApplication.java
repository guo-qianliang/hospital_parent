package com.alibaba.yygh.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: ServiceUserApplication
 * @author: gql
 * @date: 2021/11
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.alibaba.yygh")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.alibaba.yygh")
public class ServiceUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceUserApplication.class, args);
    }
}
