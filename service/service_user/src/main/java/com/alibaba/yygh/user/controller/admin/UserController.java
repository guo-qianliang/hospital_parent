package com.alibaba.yygh.user.controller.admin;

import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.model.user.UserInfo;
import com.alibaba.yygh.user.service.UserInfoService;
import com.alibaba.yygh.vo.user.UserInfoQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * description: UserController
 * @author: gql
 * @date: 2021/11
 */
@RestController
@RequestMapping("/admin/user")
public class UserController {

    @Autowired
    private UserInfoService userInfoService;


    @ApiOperation("1.用户列表-条件查询带分页")
    @GetMapping("{page}/{limit}")
    public R list(@PathVariable("page") Long page,
                  @PathVariable("limit") Long limit,
                  UserInfoQueryVo userInfoQueryVo) {
        Page<UserInfo> pageParam = new Page<>(page,limit);
        IPage<UserInfo> pageModel =
                userInfoService.selectPage(pageParam,userInfoQueryVo);
        return R.ok().data("pageModel",pageModel);
    }

    @ApiOperation(value = "2.锁定")
    @GetMapping("lock/{userId}/{status}")
    public R lock(
            @PathVariable("userId") Long userId,
            @PathVariable("status") Integer status){
        userInfoService.lock(userId, status);
        return R.ok();
    }

    @ApiOperation(value = "3.用户详情")
    @GetMapping("show/{userId}")
    public R show(@PathVariable("userId") Long userId) {
        Map<String,Object> map = userInfoService.showUser(userId);
        return R.ok().data(map);
    }

    @ApiOperation(value = "4.认证审批")
    @GetMapping("approval/{userId}/{authStatus}")
    public R approval(@PathVariable("userId") Long userId,
                      @PathVariable("authStatus") Integer authStatus) {
        userInfoService.approval(userId,authStatus);
        return R.ok();
    }
}
