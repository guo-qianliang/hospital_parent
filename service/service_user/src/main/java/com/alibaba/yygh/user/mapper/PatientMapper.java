package com.alibaba.yygh.user.mapper;

import com.alibaba.yygh.model.user.Patient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 就诊人表 Mapper 接口
 * </p>
 *
 * @author gql
 * @since 2021-11-14
 */
public interface PatientMapper extends BaseMapper<Patient> {

}
