package com.alibaba.yygh.orders.service;

import com.alibaba.yygh.model.order.PaymentInfo;
import com.alibaba.yygh.model.order.RefundInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 退款信息表 服务类
 * </p>
 *
 * @author gql
 * @since 2021-11-18
 */
public interface RefundInfoService extends IService<RefundInfo> {
    /**
     * 保存退款记录
     * @param paymentInfo 支付信息
     */
    RefundInfo saveRefundInfo(PaymentInfo paymentInfo);
}
