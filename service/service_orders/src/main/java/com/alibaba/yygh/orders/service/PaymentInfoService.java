package com.alibaba.yygh.orders.service;

import com.alibaba.yygh.model.order.OrderInfo;
import com.alibaba.yygh.model.order.PaymentInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 支付信息表 服务类
 * </p>
 *
 * @author gql
 * @since 2021-11-17
 */
public interface PaymentInfoService extends IService<PaymentInfo> {
    /**
     * 1.保存支付记录
     * @param order
     * @param paymentType 支付类型（1：微信 2：支付宝）
     */
    void savePaymentInfo(OrderInfo order, Integer paymentType);

    /**
     * 更新订单状态和支付状态
     * @param outTradeNo 交易号
     * @param paymentType 支付类型
     * @param paramMap 调用微信查询支付状态接口返回map集合
     */
    void paySuccess(String outTradeNo, Integer paymentType, Map<String, String> paramMap);
}
