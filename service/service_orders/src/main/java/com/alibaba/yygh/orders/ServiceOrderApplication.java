package com.alibaba.yygh.orders;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: ServiceOrderApplication
 * @author: gql
 * @date: 2021/11
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.alibaba.yygh"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.alibaba.yygh"})
@MapperScan("com.alibaba.yygh.orders.mapper")
public class ServiceOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceOrderApplication.class, args);
    }
}
