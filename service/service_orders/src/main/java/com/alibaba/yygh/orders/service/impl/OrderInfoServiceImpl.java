package com.alibaba.yygh.orders.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.yygh.client.HospitalFeignClient;
import com.alibaba.yygh.client.PatientFeignClient;
import com.alibaba.yygh.common.exception.YyghException;
import com.alibaba.yygh.common.result.ResultCodeEnum;
import com.alibaba.yygh.enums.OrderStatusEnum;
import com.alibaba.yygh.model.order.OrderInfo;
import com.alibaba.yygh.model.user.Patient;
import com.alibaba.yygh.orders.mapper.OrderInfoMapper;
import com.alibaba.yygh.orders.service.OrderInfoService;
import com.alibaba.yygh.orders.service.WeixinService;
import com.alibaba.yygh.orders.utils.HttpRequestHelper;
import com.alibaba.yygh.rabbit.RabbitService;
import com.alibaba.yygh.rabbit.constant.MqConst;
import com.alibaba.yygh.vo.hosp.ScheduleOrderVo;
import com.alibaba.yygh.vo.msm.MsmVo;
import com.alibaba.yygh.vo.order.OrderCountQueryVo;
import com.alibaba.yygh.vo.order.OrderCountVo;
import com.alibaba.yygh.vo.order.OrderMqVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author gql
 * @since 2021-11-16
 */
@Service
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements OrderInfoService {

    @Autowired
    private HospitalFeignClient hospitalFeignClient;

    @Autowired
    private PatientFeignClient patientFeignClient;

    @Autowired
    private RabbitService rabbitService;

    @Autowired
    private WeixinService weixinService;

    /**
     * 1.生成挂号订单
     * @param scheduleId 排班id
     * @param patientId 就诊人id
     * @return 订单号
     */
    @Override
    public Long saveOrder(String scheduleId, Long patientId) {
        // 1.远程调用就诊人信息
        Patient patient = patientFeignClient.getPatient(patientId);

        // 2.远程调用排班信息
        ScheduleOrderVo scheduleOrderVo = hospitalFeignClient.getScheduleOrderVo(scheduleId);

        // 3.使用httpclient调用医院系统接口,确认号是否存在
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("hoscode", scheduleOrderVo.getHoscode());
        paramMap.put("depcode", scheduleOrderVo.getDepcode());
        paramMap.put("hosScheduleId", scheduleOrderVo.getHosScheduleId());
        paramMap.put("reserveDate", new DateTime(scheduleOrderVo.getReserveDate()).toString("yyyy-MM-dd"));
        paramMap.put("reserveTime", scheduleOrderVo.getReserveTime());
        paramMap.put("amount", scheduleOrderVo.getAmount()); //挂号费用
        paramMap.put("name", patient.getName());
        paramMap.put("certificatesType", patient.getCertificatesType());
        paramMap.put("certificatesNo", patient.getCertificatesNo());
        paramMap.put("sex", patient.getSex());
        paramMap.put("birthdate", patient.getBirthdate());
        paramMap.put("phone", patient.getPhone());
        paramMap.put("isMarry", patient.getIsMarry());
        paramMap.put("provinceCode", patient.getProvinceCode());
        paramMap.put("cityCode", patient.getCityCode());
        paramMap.put("districtCode", patient.getDistrictCode());
        paramMap.put("address", patient.getAddress());
        //联系人
        paramMap.put("contactsName", patient.getContactsName());
        paramMap.put("contactsCertificatesType", patient.getContactsCertificatesType());
        paramMap.put("contactsCertificatesNo", patient.getContactsCertificatesNo());
        paramMap.put("contactsPhone", patient.getContactsPhone());
        paramMap.put("timestamp", HttpRequestHelper.getTimestamp());
        //String sign = HttpRequestHelper.getSign(paramMap, signInfoVo.getSignKey());
        paramMap.put("sign", "");

        // 4.使用httpclient发送请求，请求医院接口
        JSONObject result =
                HttpRequestHelper.sendRequest(paramMap, "http://localhost:9998/order/submitOrder");
        // 根据医院接口返回状态码判断  200 成功
        if (result.getInteger("code") == 200) { //挂号成功
            // 4.1 得到医院系统返回数据
            JSONObject jsonObject = result.getJSONObject("data");
            String hosRecordId = jsonObject.getString("hosRecordId");// 预约记录唯一标识（医院预约记录主键）
            Integer number = jsonObject.getInteger("number");// 预约序号
            String fetchTime = jsonObject.getString("fetchTime");// 取号时间
            String fetchAddress = jsonObject.getString("fetchAddress");// 取号地址

            // 4.2 得到就诊人信息、排班数据信息

            // 4.3 把三部分数据添加到订单表中
            OrderInfo orderInfo = new OrderInfo();
            // 将排班数据scheduleOrderVo中的值放入orderInfo
            BeanUtils.copyProperties(scheduleOrderVo, orderInfo);
            // 设置医院系统返回数据
            orderInfo.setHosRecordId(hosRecordId);
            orderInfo.setNumber(number);
            orderInfo.setFetchTime(fetchTime);
            orderInfo.setFetchAddress(fetchAddress);
            // 设置就诊人数据：流水号(时间戳+随机数)、排班id、订单状态
            String outTradeNo = System.currentTimeMillis() + "" + new Random().nextInt(100);
            orderInfo.setOutTradeNo(outTradeNo);
            orderInfo.setScheduleId(scheduleId);
            orderInfo.setOrderStatus(OrderStatusEnum.UNPAID.getStatus());// 设置
            orderInfo.setUserId(patient.getUserId());
            orderInfo.setPatientId(patientId);
            orderInfo.setPatientName(patient.getName());
            orderInfo.setPatientPhone(patient.getPhone());

            baseMapper.insert(orderInfo);

            // 根据医院返回数据，更新排班数量
            Integer reservedNumber = jsonObject.getInteger("reservedNumber");// 排班可预约数
            Integer availableNumber = jsonObject.getInteger("availableNumber");// 排班剩余预约数

            // 设置更新号源和短信通知
            OrderMqVo orderMqVo = new OrderMqVo();
            orderMqVo.setScheduleId(scheduleId);// 排班id
            orderMqVo.setReservedNumber(reservedNumber);// 可预约数量
            orderMqVo.setAvailableNumber(availableNumber);// 剩余可预约数量
            // 设置短信提示
            MsmVo msmVo = new MsmVo();
            msmVo.setPhone(patient.getPhone());// 就诊人手机号
            orderMqVo.setMsmVo(msmVo);
            //String reserveDate =
            //        new DateTime(orderInfo.getReserveDate()).toString("yyyy-MM-dd")
            //                + (orderInfo.getReserveTime()==0 ? "上午": "下午");
            //Map<String,Object> param = new HashMap<String,Object>(){{
            //    put("title", orderInfo.getHosname()+"|"+orderInfo.getDepname()+"|"+orderInfo.getTitle());
            //    put("amount", orderInfo.getAmount());
            //    put("reserveDate", reserveDate);
            //    put("name", orderInfo.getPatientName());
            //    put("quitTime", new DateTime(orderInfo.getQuitTime()).toString("yyyy-MM-dd HH:mm"));
            //}};
            //msmVo.setParam(param);

            rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_ORDER, MqConst.ROUTING_ORDER, orderMqVo);

            return orderInfo.getId();
        } else { //挂号失败
            throw new YyghException(20001, "挂号失败");
        }
    }

    /**
     * 2.查询订单详情
     * @param orderId 订单id
     * @return
     */
    @Override
    public OrderInfo getOrderInfo(Long orderId) {
        OrderInfo orderInfo = this.packOrderInfo(baseMapper.selectById(orderId));
        return orderInfo;
    }

    /**
     * 3.取消预约
     * @param orderId 订单id
     * @return
     */
    @Override
    public Boolean cancelOrder(Long orderId) {
        // 1.根据orderId查询订单信息
        OrderInfo orderInfo = this.getOrderInfo(orderId);
        // 2.封装医院系统需要的数据,使用map集合
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("hoscode", orderInfo.getHoscode());
        reqMap.put("hosRecordId", orderInfo.getHosRecordId());
        reqMap.put("timestamp", HttpRequestHelper.getTimestamp());
        reqMap.put("sign", "");
        // 3.调用医院系统接口,查看号码是否可以取消
        //如果当前时间 > 退号时间，不能取消预约
        DateTime quitTime = new DateTime(orderInfo.getQuitTime());
        if (quitTime.isBeforeNow()) {
            throw new YyghException(20001, "已超时,不能取消预约");
        }
        JSONObject result = HttpRequestHelper.sendRequest(reqMap, "http://localhost:9998/order/updateCancelStatus");

        // TODO: bug,退款时,签名校验有问题,暂时搁置,不进行判断
        // 4.根据医院返回数据判断,如果返回成功,可以取消
        //if (result.getInteger("code") != 200) {
        //    throw new YyghException(ResultCodeEnum.FAIL.getCode(), result.getString("message"));
        //} else {
        // 5.微信退款、添加退款记录
        if (orderInfo.getOrderStatus().intValue() == OrderStatusEnum.PAID.getStatus().intValue()) {
            //已支付 退款
            boolean isRefund = weixinService.refund(orderId);
            if (!isRefund) {
                throw new YyghException(20001, "退款失败");
            }
        }
        // 6.更新订单状态
        orderInfo.setOrderStatus(OrderStatusEnum.CANCLE.getStatus());
        this.updateById(orderInfo);

        // 7.发送mq消息,更新排班(号)数量
        //发送mq信息更新预约数 我们与下单成功更新预约数使用相同的mq信息，不设置可预约数与剩余预约数，接收端可预约数减1即可
        OrderMqVo orderMqVo = new OrderMqVo();
        orderMqVo.setScheduleId(orderInfo.getScheduleId());
        //短信提示
        MsmVo msmVo = new MsmVo();
        msmVo.setPhone(orderInfo.getPatientPhone());
        orderMqVo.setMsmVo(msmVo);
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_ORDER, MqConst.ROUTING_ORDER, orderMqVo);
        return true;
        //}
    }

    /**
     * 4.就医提醒
     */
    @Override
    public void patientTips() {
        QueryWrapper<OrderInfo> queryWrapper = new QueryWrapper<>();
        // 就诊日期
        queryWrapper.eq("reserve_date", new DateTime().toString("yyyy-MM-dd"));
        // 查询订单状态不为-1的数据
        queryWrapper.ne("order_status", OrderStatusEnum.CANCLE.getStatus());
        List<OrderInfo> orderInfoList = baseMapper.selectList(queryWrapper);

        for (OrderInfo orderInfo : orderInfoList) {
            MsmVo msmVo = new MsmVo();
            // 得到每个挂号人的手机号
            msmVo.setPhone(orderInfo.getPatientPhone());
            // 得到其他挂号信息
            String reserveDate = new DateTime(orderInfo.getReserveDate()).toString("yyyy-MM-dd") + (orderInfo.getReserveTime() == 0 ? "上午" : "下午");
            Map<String, Object> param = new HashMap<String, Object>() {{
                put("title", orderInfo.getHosname() + "|" + orderInfo.getDepname() + "|" + orderInfo.getTitle());
                put("reserveDate", reserveDate);
                put("name", orderInfo.getPatientName());
            }};
            msmVo.setParam(param);

            // 通过rabbitmq向短信模块发送消息
            rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_MSM, MqConst.ROUTING_MSM_ITEM, msmVo);
        }
    }

    /**
     * 4.获取订单统计数据
     * @param orderCountQueryVo
     * @return
     */
    @Override
    public Map<String, Object> getCountMap(OrderCountQueryVo orderCountQueryVo) {
        List<OrderCountVo> orderCountVoList = baseMapper.selectOrderCount(orderCountQueryVo);

        // 统计日期列表
        List<String> dateList = orderCountVoList.stream().map(OrderCountVo::getReserveDate).collect(Collectors.toList());
        // 统计数量列表
        List<Integer> countList = orderCountVoList.stream().map(OrderCountVo::getCount).collect(Collectors.toList());

        Map<String, Object> map = new HashMap<>();
        map.put("dateList", dateList);// x轴 日期
        map.put("countList", countList);// y轴 数量
        return map;
    }

    /**
     * 对订单信息中的状态码对应的值进行匹配
     * @param orderInfo
     * @return
     */
    private OrderInfo packOrderInfo(OrderInfo orderInfo) {
        /**
         *  0,"预约成功，待支付"
         *  1,"已支付"
         *  2,"已取号"
         *  -1,"取消预约"
         */
        orderInfo.getParam().put("orderStatusString", OrderStatusEnum.getStatusNameByStatus(orderInfo.getOrderStatus()));
        return orderInfo;
    }
}
