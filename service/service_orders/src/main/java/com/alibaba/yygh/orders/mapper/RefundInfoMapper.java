package com.alibaba.yygh.orders.mapper;

import com.alibaba.yygh.model.order.RefundInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 退款信息表 Mapper 接口
 * </p>
 *
 * @author gql
 * @since 2021-11-18
 */
public interface RefundInfoMapper extends BaseMapper<RefundInfo> {

}
