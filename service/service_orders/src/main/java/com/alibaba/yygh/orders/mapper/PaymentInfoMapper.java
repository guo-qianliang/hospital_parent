package com.alibaba.yygh.orders.mapper;

import com.alibaba.yygh.model.order.PaymentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付信息表 Mapper 接口
 * </p>
 *
 * @author gql
 * @since 2021-11-17
 */
public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {

}
