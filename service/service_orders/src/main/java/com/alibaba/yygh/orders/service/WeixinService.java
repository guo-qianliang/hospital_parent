package com.alibaba.yygh.orders.service;

import java.util.Map;

/**
 * description: WeixinService
 * @author: gql
 * @date: 2021/11
 */
public interface WeixinService {
    /**
     * 生成二维码
     * @param orderId 订单号
     * @return map集合
     */
    Map createNative(Long orderId);

    /**
     * 查询支付状态-去微信第三方根据订单号查询
     * @param orderId 订单号
     * @param paymentType 支付类型
     * @return
     */
    Map queryPayStatus(Long orderId, String paymentType);

    /**
     * 微信退款
     * @param orderId 订单id
     * @return
     */
    Boolean refund(Long orderId);
}
