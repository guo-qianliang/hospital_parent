package com.alibaba.yygh.orders.service;

import com.alibaba.yygh.model.order.OrderInfo;
import com.alibaba.yygh.vo.order.OrderCountQueryVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author gql
 * @since 2021-11-16
 */
public interface OrderInfoService extends IService<OrderInfo> {
    /**
     * 1.生成挂号订单
     * @param scheduleId 排班id
     * @param patientId 就诊人id
     * @return 订单号
     */
    Long saveOrder(String scheduleId, Long patientId);

    /**
     * 2.查询订单详情
     * @param orderId 订单id
     * @return
     */
    OrderInfo getOrderInfo(Long orderId);

    /**
     * 3.取消预约
     * @param orderId 订单id
     * @return
     */
    Boolean cancelOrder(Long orderId);

    /**
     * 4.就医提醒
     */
    void patientTips();

    /**
     * 4.获取订单统计数据
     * @param orderCountQueryVo
     * @return
     */
    Map<String, Object> getCountMap(OrderCountQueryVo orderCountQueryVo);
}
