package com.alibaba.yygh.orders.controller;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowItem;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRuleManager;
import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.model.order.OrderInfo;
import com.alibaba.yygh.orders.service.OrderInfoService;
import com.alibaba.yygh.vo.order.OrderCountQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author gql
 * @since 2021-11-16
 */
@Api(tags = "订单接口")
@RestController
@RequestMapping("/api/order/orderInfo")
public class OrderInfoController {
    @Autowired
    private OrderInfoService orderInfoService;


    // 无参构造方法
    public OrderInfoController() {
        initRule();
    }

    /**
     * 导入热点值限流规则
     * 也可在Sentinel dashboard界面配置（仅测试）
     */
    public void initRule() {
        ParamFlowRule pRule = new ParamFlowRule("submitOrder")//资源名称，与SentinelResource值保持一致
                //限流第一个参数
                .setParamIdx(0)
                //单机阈值
                .setCount(5);

        // 针对 热点参数值单独设置限流 QPS 阈值，而不是全局的阈值.
        //如：1000（北京协和医院）,可以通过数据库表一次性导入，目前为测试
        ParamFlowItem item1 = new ParamFlowItem().setObject("10000")//热点值
                .setClassType(String.class.getName())//热点值类型
                .setCount(1);//热点值 QPS 阈值
        List<ParamFlowItem> list = new ArrayList<>();
        list.add(item1);
        pRule.setParamFlowItemList(list);
        ParamFlowRuleManager.loadRules(Collections.singletonList(pRule));
    }

    // TODO 为了测试热点值规则
    @ApiOperation(value = "创建订单")
    @PostMapping("auth/submitOrder/{hoscode}/{scheduleId}/{patientId}")
    // 多层路径时,使用@SentinelResource注解自己设置资源名称进行匹配,超过阈值之后,进行热点值控制时执行blockHandler方法
    @SentinelResource(value = "submitOrder",blockHandler = "submitOrderBlockHandler")
    public R submitOrder(
            @ApiParam(name = "hoscode", value = "医院编号，限流使用", required = true)
            @PathVariable String hoscode,
            @ApiParam(name = "scheduleId", value = "排班id", required = true)
            @PathVariable String scheduleId,
            @ApiParam(name = "patientId", value = "就诊人id", required = true)
            @PathVariable Long patientId) {
        //调用service方法
        //返回订单号
        Long orderId = 1L; //orderService.saveOrders(scheduleId,patientId);
        return R.ok().data("orderId",orderId);
    }

    /**
     * 热点值超过 QPS 阈值，返回结果
     * 兜底方法: 参数与接口参数保持一致,并添加额外的BlockException参数
     * @param hoscode
     * @param scheduleId
     * @param patientId
     * @param e
     * @return
     */
    public R submitOrderBlockHandler(String hoscode, String scheduleId, Long patientId, BlockException e){
        return R.error().message("系统业务繁忙，请稍后下单");
    }

    @ApiOperation(value = "1.生成挂号订单")
    @PostMapping("auth/submitOrder/{scheduleId}/{patientId}")
    public R submitOrder(
            @PathVariable String scheduleId,
            @PathVariable Long patientId) {

        Long orderId = orderInfoService.saveOrder(scheduleId, patientId);
        return R.ok().data("orderId", orderId);
    }

    @ApiOperation(value = "2.查询订单详情-根据订单id")
    @GetMapping("auth/getOrders/{orderId}")
    public R getOrders(@PathVariable Long orderId) {
        OrderInfo orderInfo = orderInfoService.getOrderInfo(orderId);
        return R.ok().data("orderInfo", orderInfo);
    }

    @ApiOperation(value = "3.取消预约")
    @GetMapping("auth/cancelOrder/{orderId}")
    public R cancelOrder(@PathVariable("orderId") Long orderId) {
        Boolean flag = orderInfoService.cancelOrder(orderId);
        return R.ok().data("flag", flag);
    }

    @ApiOperation(value = "4.获取订单统计数据")
    @PostMapping("inner/getCountMap")
    public Map<String, Object> getCountMap(@RequestBody OrderCountQueryVo orderCountQueryVo) {
        return orderInfoService.getCountMap(orderCountQueryVo);
    }
}

