package com.alibaba.yygh.orders.service.impl;

import com.alibaba.yygh.common.exception.YyghException;
import com.alibaba.yygh.enums.OrderStatusEnum;
import com.alibaba.yygh.enums.PaymentStatusEnum;
import com.alibaba.yygh.model.order.OrderInfo;
import com.alibaba.yygh.model.order.PaymentInfo;
import com.alibaba.yygh.orders.mapper.PaymentInfoMapper;
import com.alibaba.yygh.orders.service.OrderInfoService;
import com.alibaba.yygh.orders.service.PaymentInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 支付信息表 服务实现类
 * </p>
 *
 * @author gql
 * @since 2021-11-17
 */
@Service
public class PaymentInfoServiceImpl extends ServiceImpl<PaymentInfoMapper, PaymentInfo> implements PaymentInfoService {

    @Autowired
    private OrderInfoService orderInfoService;

    /**
     * 1.保存交易记录
     * @param order
     * @param paymentType 支付类型（1：微信 2：支付宝）
     */
    @Override
    public void savePaymentInfo(OrderInfo order, Integer paymentType) {
        // 1.根据orderId查询支付记录是否存在
        QueryWrapper<PaymentInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id", order.getId());
        wrapper.eq("payment_type", paymentType);
        Integer count = baseMapper.selectCount(wrapper);
        if (count > 0) {
            // 如果存在就不需要再添加
            return;
        }
        // 2.添加交易记录-如果不存在
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setCreateTime(new Date());
        paymentInfo.setOrderId(order.getId());// 订单号
        paymentInfo.setPaymentType(paymentType);// 类型
        paymentInfo.setOutTradeNo(order.getOutTradeNo());// 流水号
        paymentInfo.setPaymentStatus(PaymentStatusEnum.UNPAID.getStatus());// 支付状态

        String subject = new DateTime(order.getReserveDate()).toString("yyyy-MM-dd") + "|" + order.getHosname() + "|" + order.getDepname() + "|" + order.getTitle();
        paymentInfo.setSubject(subject);// 交易内容信息
        paymentInfo.setTotalAmount(order.getAmount());// 挂号费
        baseMapper.insert(paymentInfo);

    }

    /**
     * 更新订单状态和支付状态
     * @param outTradeNo 交易号
     * @param paymentType 支付类型
     * @param paramMap 调用微信查询支付状态接口返回map集合
     */
    @Override
    public void paySuccess(String outTradeNo, Integer paymentType, Map<String, String> paramMap) {
        //1.更新订单状态为已支付
        QueryWrapper<OrderInfo> wrapperOrder = new QueryWrapper<>();
        wrapperOrder.eq("out_trade_no",outTradeNo);
        OrderInfo orderInfo = orderInfoService.getOne(wrapperOrder);
        if(null == orderInfo){
            throw new YyghException(20001,"根据此交易号无法查到订单信息");
        }
        orderInfo.setOrderStatus(OrderStatusEnum.PAID.getStatus());
        orderInfoService.updateById(orderInfo);

        //2.更新支付记录状态为已支付
        QueryWrapper<PaymentInfo> wrapperPayment = new QueryWrapper<>();
        wrapperPayment.eq("out_trade_no",outTradeNo);
        PaymentInfo paymentInfo = baseMapper.selectOne(wrapperPayment);
        if(null == paymentInfo){
            throw new YyghException(20001,"根据此交易号无法查到支付信息");
        }
        paymentInfo.setPaymentStatus(PaymentStatusEnum.PAID.getStatus());
        paymentInfo.setTradeNo(paramMap.get("transaction_id"));// 事务id,由微信支付状态接口返回,退款时使用
        paymentInfo.setCallbackTime(new Date());
        paymentInfo.setCallbackContent(paramMap.toString());
        baseMapper.updateById(paymentInfo);
    }
}
