package com.alibaba.yygh.orders.service.impl;

import com.alibaba.yygh.enums.RefundStatusEnum;
import com.alibaba.yygh.model.order.PaymentInfo;
import com.alibaba.yygh.model.order.RefundInfo;
import com.alibaba.yygh.orders.mapper.RefundInfoMapper;
import com.alibaba.yygh.orders.service.RefundInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 退款信息表 服务实现类
 * </p>
 *
 * @author gql
 * @since 2021-11-18
 */
@Service
public class RefundInfoServiceImpl extends ServiceImpl<RefundInfoMapper, RefundInfo> implements RefundInfoService {

    /**
     * 添加退款记录
     * @param paymentInfo 支付信息
     */
    @Override
    public RefundInfo saveRefundInfo(PaymentInfo paymentInfo) {
        // 1. 根据订单号查询退款记录时候存在
        QueryWrapper<RefundInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", paymentInfo.getOrderId());
        RefundInfo refundInfo = baseMapper.selectOne(queryWrapper);
        if(null != refundInfo){// 存在
            return refundInfo;
        }
        // 不存在-保存交易记录
        refundInfo = new RefundInfo();
        refundInfo.setCreateTime(new Date());//
        refundInfo.setOrderId(paymentInfo.getOrderId());
        refundInfo.setPaymentType(paymentInfo.getPaymentType());
        refundInfo.setOutTradeNo(paymentInfo.getOutTradeNo());
        refundInfo.setRefundStatus(RefundStatusEnum.UNREFUND.getStatus());
        refundInfo.setSubject(paymentInfo.getSubject());
        //paymentInfo.setSubject("test");
        refundInfo.setTotalAmount(paymentInfo.getTotalAmount());

        baseMapper.insert(refundInfo);
        return refundInfo;
    }
}
