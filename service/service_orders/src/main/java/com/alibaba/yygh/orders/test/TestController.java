package com.alibaba.yygh.orders.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * description: TestController
 * @author: gql
 * @date: 2021/11
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping
    public String test1() {
        return "hello";
    }
}
