package com.alibaba.yygh.orders.mapper;

import com.alibaba.yygh.model.order.OrderInfo;
import com.alibaba.yygh.vo.order.OrderCountQueryVo;
import com.alibaba.yygh.vo.order.OrderCountVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author gql
 * @since 2021-11-16
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

    /**
     * 统计每天平台预约数据
     * @param orderCountQueryVo
     * @return
     */
    List<OrderCountVo> selectOrderCount(OrderCountQueryVo orderCountQueryVo);
}
