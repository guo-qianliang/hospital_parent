package com.alibaba.yygh.msm.service.impl;

import com.alibaba.yygh.msm.service.MsmService;
import com.alibaba.yygh.msm.utils.HttpUtils;
import com.alibaba.yygh.msm.utils.RandomUtil;
import com.alibaba.yygh.vo.msm.MsmVo;
import org.apache.http.HttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * description: MsmServiceImpl
 * @author: gql
 * @date: 2021/11
 */
@Service
public class MsmServiceImpl implements MsmService {

    /**
     * 发送短信-使用第三方国阳云
     * @param phoneNumbers 手机号码
     * @param param 4位或6位数字的验证码
     * @throws Exception
     */
    @Override
    public boolean sendShortMessage(String phoneNumbers, String param) throws Exception {
        String host = "https://gyytz.market.alicloudapi.com";
        String path = "/sms/smsSend";
        String method = "POST";
        String appcode = "fc81e81d63ee4bbb88f07c5246b48137";
        Map<String, String> headers = new HashMap<String, String>();
        // 最后在header中的格式(中间是英文空格)为Authorization:APPCODE appcodexxx
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("mobile", phoneNumbers);
        String getParam = param;
        //System.out.println("getParam = " + getParam);
        querys.put("param", "**code**:" + getParam + ",**minute**:5");
        querys.put("smsSignId", "2e65b1bb3d054466b82f0c9d125465e2");
        querys.put("templateId", "908e94ccf08b4476ba6c876d13f084ad");
        Map<String, String> bodys = new HashMap<String, String>();

        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            System.out.println(response.toString());
            return true;
            //获取response的body
            //System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 下单发送短信提醒
     * @param msmVo
     */
    @Override
    public boolean send(MsmVo msmVo) throws Exception {
        String phone = msmVo.getPhone();
        if (!StringUtils.isEmpty(phone)) {
            // 给就诊人发送短信
            String code = RandomUtil.getFourBitRandom();
            System.out.println("短信发送给了 = " + phone);
            return this.sendShortMessage(phone, code);
        }
        return false;
    }

}
