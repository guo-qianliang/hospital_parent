package com.alibaba.yygh.msm.receiver;

import com.alibaba.yygh.msm.service.MsmService;
import com.alibaba.yygh.rabbit.constant.MqConst;
import com.alibaba.yygh.vo.msm.MsmVo;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * description: SmsReceiver MQ监听器
 * @author: gql
 * @date: 2021/11
 */
@Component
public class SmsReceiver {
    @Autowired
    private MsmService msmService;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = MqConst.QUEUE_MSM_ITEM, durable = "true"),
            exchange = @Exchange(value = MqConst.EXCHANGE_DIRECT_MSM),
            key = {MqConst.ROUTING_MSM_ITEM}
    ))
    public void send(MsmVo msmVo, Message message, Channel channel) {
        try {
            boolean send = msmService.send(msmVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
