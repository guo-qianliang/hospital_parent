package com.alibaba.yygh.msm.service;

import com.alibaba.yygh.vo.msm.MsmVo;

/**
 * description: MsmService
 * @author: gql
 * @date: 2021/11
 */
public interface MsmService {
    /**
     * 1.登录发送短信验证码
     * @param phone
     * @param code
     * @return
     */
    boolean sendShortMessage(String phone, String code) throws Exception;

    /**
     * 2.下单mq发送短信提醒
     * @param msmVo
     */
    boolean send(MsmVo msmVo) throws Exception;
}
