package com.alibaba.yygh.msm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: ServiceMsmApplication
 * @author: gql
 * @date: 2021/11
 */
@ComponentScan({"com.alibaba.yygh"})
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)//取消寻找数据源
public class ServiceMsmApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceMsmApplication.class, args);
    }
}
