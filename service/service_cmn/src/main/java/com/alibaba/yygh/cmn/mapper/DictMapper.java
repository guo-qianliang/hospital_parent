package com.alibaba.yygh.cmn.mapper;

import com.alibaba.yygh.model.cmn.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * description: DictMapper
 * @author: gql
 * @date: 2021/11
 */
@Repository
public interface DictMapper extends BaseMapper<Dict> {
}
