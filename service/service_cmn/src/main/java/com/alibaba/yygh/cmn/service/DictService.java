package com.alibaba.yygh.cmn.service;

import com.alibaba.yygh.model.cmn.Dict;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * description: DictService
 * @author: gql
 * @date: 2021/11
 */
public interface DictService extends IService<Dict> {
    /**
     * 1.根据数据id查询子数据列表
     * @param id
     * @return
     */
    List<Dict> findChlidData(Long id);

    /**
     * 2.导出Excel
     * @param response
     */
    void exportData(HttpServletResponse response);

    /**
     * 3.导入Excel
     * @param file
     */
    void importDictData(MultipartFile file);


    /**
     * 4.根据dictCode与value获取数据字典名称
     * @param dictCode
     * @param value
     */
    String getNameByDictCodeAndValue(String dictCode, String value);

    /**
     * 5.根据dictCode查询下级节点数据
     * @param dictCode
     * @return
     */
    List<Dict> findByDictCode(String dictCode);
}
