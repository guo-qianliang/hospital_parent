package com.alibaba.yygh.cmn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: ServiceCmnApplication 数据字典启动类
 * @author: gql
 * @date: 2021/11
 */

@SpringBootApplication
// 注册到Nacos
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.alibaba.yygh"})
public class ServiceCmnApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceCmnApplication.class, args);
    }
}
