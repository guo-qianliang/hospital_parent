package com.alibaba.yygh.cmn.controller;

import com.alibaba.yygh.cmn.service.DictService;
import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.model.cmn.Dict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * description: DictController
 * @author: gql
 * @date: 2021/11
 */
@Api(description = "数据字典接口")
@RestController
@RequestMapping("/admin/cmn/dict")
//@CrossOrigin
public class DictController {

    @Autowired
    private DictService dictService;

    @ApiOperation(value = "1.根据数据id查询子数据列表")
    @GetMapping("findChildData/{id}")
    public R findChildData(@PathVariable Long id) {
        List<Dict> list = dictService.findChlidData(id);
        return R.ok().data("list", list);
    }

    @ApiOperation(value = "2.导出Excel")
    @GetMapping(value = "exportData")
    public void exportData(HttpServletResponse response) {
        dictService.exportData(response);
    }

    @ApiOperation(value = "3.导入Excel")
    @PostMapping("importData")
    // 注意：形参必须是file,这是element-ui底层封装提供的变量名
    public R importData(MultipartFile file) {
        dictService.importDictData(file);
        return R.ok();
    }


    @ApiOperation(value = "4.1 根据dictCode和value获取数据字典名称")
    @GetMapping(value = "getName/{dictCode}/{value}")
    public String getName(
            @PathVariable("dictCode") String dictCode,
            @PathVariable("value") String value) {
        return dictService.getNameByDictCodeAndValue(dictCode, value);
    }

    @ApiOperation(value = "4.2 根据value获取数据字典名称")
    @GetMapping(value = "getName/{value}")
    public String getName(
            @PathVariable("value") String value) {
        return dictService.getNameByDictCodeAndValue("", value);
    }

    @ApiOperation(value = "5.根据dictCode查询下级节点")
    @GetMapping(value = "findByDictCode/{dictCode}")
    public R findByDictCode(@PathVariable("dictCode") String dictCode) {
        List<Dict> list = dictService.findByDictCode(dictCode);
        return R.ok().data("list",list);
    }
}
