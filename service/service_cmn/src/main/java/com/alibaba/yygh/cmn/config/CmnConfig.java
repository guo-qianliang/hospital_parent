package com.alibaba.yygh.cmn.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * description: CmnConfig
 * @author: gql
 * @date: 2021/11
 */
@Configuration
@MapperScan("com.alibaba.yygh.cmn.mapper")
public class CmnConfig {

}
