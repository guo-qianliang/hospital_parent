package com.alibaba.read;

import com.alibaba.entity.Student;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * description: ReadExcel
 * @author: gql
 * @date: 2021/11
 */
public class ReadExcel {
    public static void main(String[] args) {
        String fileName = "E:\\ExcelDemo\\1.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        EasyExcel.read(fileName, Student.class, new ExcelListener()).sheet().doRead();
    }
}

/**
 *
 */
class ExcelListener extends AnalysisEventListener<Student> {

    //创建list集合封装最终的数据
    List<Student> list = new ArrayList<Student>();

    /**
     * 读表头
     * @param headMap
     * @param context
     */
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头信息：" + headMap);
    }

    /**
     * 从第二行开始,一行一行读内容
     * @param user
     * @param analysisContext
     */
    @Override
    public void invoke(Student user, AnalysisContext analysisContext) {
        System.out.println("***" + user);
        list.add(user);
    }


    /**
     * 读取完成后执行
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
