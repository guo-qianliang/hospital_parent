package com.alibaba.write;

import com.alibaba.entity.Student;
import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

/**
 * description: WriteExcel 写操作
 * @author: gql
 * @date: 2021/11
 */
public class WriteExcel {
    public static void main(String[] args) {
        // 要操作的Excel的路径
        String fileName = "E:\\ExcelDemo\\1.xlsx";
        /**
         * write(args1,args2)
         *      args1:excel文件路径和文件名称;
         *      args2:excel表头对应的实体类;
         */
        EasyExcel.write(fileName, Student.class).sheet("学生信息").doWrite(data());
    }

    /**
     * 构造数据
     * @return list
     */
    private static List<Student> data() {
        List<Student> list = new ArrayList<Student>();
        for (int i = 0; i < 10; i++) {
            Student student = new Student();
            student.setSno(i);
            student.setSname("张三" + i);
            list.add(student);
        }
        return list;
    }
}
