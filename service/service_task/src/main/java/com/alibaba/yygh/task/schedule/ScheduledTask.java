package com.alibaba.yygh.task.schedule;

import com.alibaba.yygh.rabbit.RabbitService;
import com.alibaba.yygh.rabbit.constant.MqConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * description: ScheduledTask 消息队列的发送端
 * @author: gql
 * @date: 2021/11
 */
@Component
// 开启定时任务
@EnableScheduling
public class ScheduledTask {

    @Autowired
    private RabbitService rabbitService;
    /**
     * 每天8点执行 提醒就诊
     */
    //@Scheduled(cron = "0 0 8 * * ?")
    @Scheduled(cron = "0/10 * * * * ?")
    public void task1() {
        System.out.println("定时器执行,当前时间是:"+new Date().toLocaleString());
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_TASK, MqConst.ROUTING_TASK_8, "");
    }
}
