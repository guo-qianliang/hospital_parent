package com.alibaba.yygh.hosp.repository;

import com.alibaba.yygh.model.hosp.Hospital;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * description: HospitalRepository
 * @author: gql
 * @date: 2021/11
 */
@Repository
public interface HospitalRepository extends MongoRepository<Hospital,String> {
    /**
     * 1.根据Hosecode查询医院信息
     * @param hoscode
     * @return 医院对象
     */
    Hospital findByHoscode(String hoscode);

    /**
     * 2.根据医院名称获取医院列表
     * @param hosname
     * @return
     */
    List<Hospital> findByHosnameLike(String hosname);
}
