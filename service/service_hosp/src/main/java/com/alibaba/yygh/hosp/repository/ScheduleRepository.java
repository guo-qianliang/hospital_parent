package com.alibaba.yygh.hosp.repository;

import com.alibaba.yygh.model.hosp.Schedule;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * description: ScheduleRepository
 * @author: gql
 * @date: 2021/11
 */
@Repository
public interface ScheduleRepository extends MongoRepository<Schedule,String> {
    /**
     * 根据hoscode和hosScheduleId查询科室信息
     * @param hoscode
     * @param hosScheduleId
     * @return
     */
    Schedule getScheduleByHoscodeAndHosScheduleId(String hoscode, String hosScheduleId);

    /**
     * 根据hoscode和hosScheduleId删除排班信息
     * @param hoscode 医院编码
     * @param hosScheduleId 医院排班id
     */
    Schedule findScheduleByHoscodeAndHosScheduleId(String hoscode, String hosScheduleId);

    /**
     * 根据医院编号、科室编号、工作日期查询MongoDB
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @param toDate 工作日期
     * @return
     */
    List<Schedule> findScheduleByHoscodeAndDepcodeAndWorkDate(String hoscode, String depcode, Date toDate);
}
