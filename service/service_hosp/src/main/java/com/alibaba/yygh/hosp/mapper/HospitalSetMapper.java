package com.alibaba.yygh.hosp.mapper;

import com.alibaba.yygh.model.hosp.Hospital;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.alibaba.yygh.model.hosp.HospitalSet;

/**
 * <p>
 * 医院设置表 Mapper 接口
 * </p>
 *
 * @author gql
 * @since 2021-10-31
 */
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {

}
