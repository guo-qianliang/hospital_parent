package com.alibaba.yygh.hosp.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * description: HospConfig
 * @author: gql
 * @date: 2021/10
 */
@Configuration
@MapperScan("com.alibaba.yygh.hosp.mapper")
public class HospConfig {
    /**
     * mp的分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

}
