package com.alibaba.yygh.hosp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.yygh.common.exception.YyghException;
import com.alibaba.yygh.hosp.repository.ScheduleRepository;
import com.alibaba.yygh.hosp.service.DepartmentService;
import com.alibaba.yygh.hosp.service.HospitalService;
import com.alibaba.yygh.hosp.service.ScheduleService;
import com.alibaba.yygh.model.hosp.BookingRule;
import com.alibaba.yygh.model.hosp.Department;
import com.alibaba.yygh.model.hosp.Hospital;
import com.alibaba.yygh.model.hosp.Schedule;
import com.alibaba.yygh.vo.hosp.BookingScheduleRuleVo;
import com.alibaba.yygh.vo.hosp.ScheduleOrderVo;
import com.alibaba.yygh.vo.hosp.ScheduleQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * description: ScheduleServiceImpl
 * @author: gql
 * @date: 2021/11
 */
@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    /**
     * 上传排班信息
     * @param paramMap
     */
    @Override
    public void save(Map<String, Object> paramMap) {
        // 1.使用fastjson将Map转换为JSON字符串再转换为对象
        Schedule schedule = JSONObject.parseObject(JSONObject.toJSONString(paramMap), Schedule.class);

        // 2.查询科室信息是否已经添加
        Schedule existSchedule = scheduleRepository.
                getScheduleByHoscodeAndHosScheduleId(schedule.getHoscode(), schedule.getHosScheduleId());
        if (existSchedule != null) {
            // 修改
            schedule.setId(existSchedule.getId());
            schedule.setStatus(1);
            schedule.setCreateTime(new Date());
            schedule.setUpdateTime(new Date());
            scheduleRepository.save(schedule);
        } else {
            // 添加
            schedule.setCreateTime(new Date());
            schedule.setUpdateTime(new Date());
            schedule.setIsDeleted(0);
            schedule.setStatus(1);
            scheduleRepository.save(schedule);
        }
    }

    /**
     * 查询排班带分页
     * @param page 当前页
     * @param limit 每页记录数
     * @param scheduleQueryVo 查询条件
     * @return
     */
    @Override
    public Page<Schedule> selectPage(int page, int limit, ScheduleQueryVo scheduleQueryVo) {
        // 1.设置排序规则
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");

        // 2.设置分页数据
        Pageable pageable = PageRequest.of(page - 1, limit, sort);

        // 3.条件匹配器
        ExampleMatcher matcher = ExampleMatcher.matching() //构建对象
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING) //改变默认字符串匹配方式：模糊查询
                .withIgnoreCase(true); //改变默认大小写忽略方式：忽略大小写

        // 4.将scheduleQueryVo转换为schedule
        Schedule schedule = new Schedule();
        BeanUtils.copyProperties(scheduleQueryVo, schedule);
        schedule.setIsDeleted(0);

        // 5.封装查询条件
        Example<Schedule> example = Example.of(schedule, matcher);

        // 6.执行查询
        Page<Schedule> pages = this.scheduleRepository.findAll(example, pageable);

        return pages;
    }

    /**
     * 根据hoscode和hosScheduleId删除排班信息
     * @param hoscode 医院编码
     * @param hosScheduleId 医院排班id
     */
    @Override
    public void remove(String hoscode, String hosScheduleId) {
        // 1.先根据hoscode和depcode查询到排班信息
        Schedule schedule = this.scheduleRepository.findScheduleByHoscodeAndHosScheduleId(hoscode, hosScheduleId);

        // 2.如果不为空才根据id删除
        if (null != schedule) {
            this.scheduleRepository.deleteById(schedule.getId());
        }
    }

    // mongoTemplate的特点是查询方便
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private HospitalService hospitalService;

    /**
     * 查询排班规则数据-带分页
     * @param page 当前页
     * @param limit 每页记录数
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @return
     */
    @Override
    public Map<String, Object> getRuleSchedule(long page, long limit, String hoscode, String depcode) {
        // 1. 封装条件
        Criteria criteria = Criteria.where("hoscode").is(hoscode).and("depcode").is(depcode);

        // 2.1 第一次分页查询 : 封装聚合
        Aggregation agg = Aggregation.newAggregation(
                // 2.1 匹配条件
                Aggregation.match(criteria),
                // 2.3 分组字段
                Aggregation.group("workDate").first("workDate").as("workDate")
                        // 医生数量
                        .count().as("docCount")
                        // 可预约数量
                        .sum("reservedNumber").as("reservedNumber")
                        // 剩余预约数量
                        .sum("availableNumber").as("availableNumber"),
                // 2.4 排序
                Aggregation.sort(Sort.Direction.ASC, "workDate"),
                // 2.5 实现分页
                Aggregation.skip((page - 1) * limit),
                Aggregation.limit(limit)
        );

        /**
         * 2.2 调用方法实现聚合查询
         *      参数1: 封装聚合条件的对象
         *      参数2:集合对应的实体类class
         *      参数3:返回结果类型的class
         *
         * 最终返回的是一个结果集合,需要再次获取到list集合
         */
        AggregationResults<BookingScheduleRuleVo> aggResults =
                mongoTemplate.aggregate(agg, Schedule.class, BookingScheduleRuleVo.class);
        List<BookingScheduleRuleVo> bookingScheduleRuleVoList = aggResults.getMappedResults();

        // 3.1 第二次直接查询: 查询科室排班数据总记录数
        Aggregation totalAgg = Aggregation.newAggregation(
                Aggregation.match(criteria),
                Aggregation.group("workDate")
        );

        /**
         * 3.2 调用方法实现聚合查询
         *      参数1: 封装聚合条件的对象
         *      参数2:集合对应的实体类class
         *      参数3:返回结果类型的class
         *
         * 最终返回的是一个结果集合,需要再次获取到list集合
         */
        AggregationResults<BookingScheduleRuleVo> totalAggResults =
                mongoTemplate.aggregate(totalAgg, Schedule.class, BookingScheduleRuleVo.class);
        List<BookingScheduleRuleVo> bookingScheduleRuleVoList1 = totalAggResults.getMappedResults();
        // 得到科室排班数据的总记录数
        int total = bookingScheduleRuleVoList1.size();

        // 4.根据workDate获取到星期进行封装
        for (BookingScheduleRuleVo bookingScheduleRuleVo : bookingScheduleRuleVoList) {
            // 4.1 获取每个workDate
            Date workDate = bookingScheduleRuleVo.getWorkDate();
            // 4.2 调用方法,根据日期workDate获取周几数据
            String dayOfWeek = this.getDayOfWeek(new DateTime(workDate));
            // 4.3 封装到bookingScheduleRuleVo中
            bookingScheduleRuleVo.setDayOfWeek(dayOfWeek);
        }

        // 5.将两次查询到的信息设置到map集合result
        Map<String, Object> result = new HashMap<>();
        result.put("bookingScheduleRuleList", bookingScheduleRuleVoList);
        result.put("total", total);

        /**
         * 6.为result集合添加医院名称信息
         * 获取医院名称,并将医院名称放入baseMap中,然后将baseMap放入result
         */
        String hosname = hospitalService.getHospitalByHoscode(hoscode).getHosname();
        Map<String, String> baseMap = new HashMap<>();
        baseMap.put("hosname", hosname);
        result.put("baseMap", baseMap);
        return result;
    }

    /**
     * 2.查询每日排班详细信息
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @param workDate 工作日期
     * @return
     */
    @Override
    public List<Schedule> getDetailSchedule(String hoscode, String depcode, String workDate) {
        /**
         * 1.根据医院编号、科室编号、工作日期查询MongoDB
         */
        List<Schedule> scheduleList =
                // 将String类型转换为Date类型
                scheduleRepository.findScheduleByHoscodeAndDepcodeAndWorkDate(hoscode, depcode, new DateTime(workDate).toDate());
        // 2.把得到list集合遍历,封装并设置其他值：医院名称、科室名称、日期对应的星期
        scheduleList.stream().forEach(item -> {
            this.packageSchedule(item);
        });
        return scheduleList;
    }

    /**
     * 5.获取可预约排班数据
     * @param page 当前页
     * @param limit 每页记录数
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @return
     */
    @Override
    public Map<String, Object> getBookingScheduleRule(Integer page,
                                                      Integer limit,
                                                      String hoscode,
                                                      String depcode) {

        // 1.获取预约规则-根据医院编号
        Hospital hospital = hospitalService.getHospitalByHoscode(hoscode);
        if (null == hospital) {
            throw new YyghException(20001, "医院信息为空");
        }
        BookingRule bookingRule = hospital.getBookingRule();

        // 2.获取可预约日期分页数据
        IPage iPage = this.getListDate(page, limit, bookingRule);

        // 3.获取每页显示日期数据
        List<Date> dateList = iPage.getRecords();

        // 4.查询日期是否有号-根据医院编号、科室编号、工作日期
        Criteria criteria = Criteria.where("hoscode").is(hoscode)
                .and("depcode").is(depcode)
                .and("workDate").in(dateList);
        // MongoDB聚合条件封装
        Aggregation agg = Aggregation.newAggregation(
                // 匹配
                Aggregation.match(criteria),
                // 分组
                Aggregation.group("workDate")
                        .first("workDate").as("workDate")
                        // 统计
                        .count().as("docCount")
                        // 剩余预约数求和
                        .sum("reservedNumber").as("reservedNumber")
                        // 可预约数求和
                        .sum("availableNumber").as("availableNumber")
        );
        /**
         * 集合对象、实体类class、返回结果class
         */
        AggregationResults<BookingScheduleRuleVo> aggregationResults =
                mongoTemplate.aggregate(agg, Schedule.class, BookingScheduleRuleVo.class);

        // 5.得到数据集合
        List<BookingScheduleRuleVo> scheduleVoList = aggregationResults.getMappedResults();

        /**
         * 6.将所有显示日期 到 scheduleVoList,进行日期比对,如果日期有数据就显示有号,否则显示无号
         * 把查询出来的scheduleVoList集合转换为map集合,key是日期,value是日期对应的数据
         *
         */

        Map<Date, BookingScheduleRuleVo> scheduleVoMap = new HashMap<>();

        if (!CollectionUtils.isEmpty(scheduleVoList)) {
            scheduleVoMap = scheduleVoList.stream()
                    // toMap(key,value)
                    .collect(Collectors.toMap(BookingScheduleRuleVo::getWorkDate,
                            BookingScheduleRuleVo -> BookingScheduleRuleVo));
        }

        //获取可预约排班规则
        List<BookingScheduleRuleVo> bookingScheduleRuleVoList = new ArrayList<>();

        // 7.遍历dateList,根据日期到map查询是否有号,然后进行数据设置
        for (int i = 0, len = dateList.size(); i < len; i++) {
            // 得到每个日期
            Date date = dateList.get(i);
            // 用每个日期去查询map
            BookingScheduleRuleVo bookingScheduleRuleVo = scheduleVoMap.get(date);
            // 如果当天无号
            if (null == bookingScheduleRuleVo) {
                bookingScheduleRuleVo = new BookingScheduleRuleVo();
                //就诊医生人数
                bookingScheduleRuleVo.setDocCount(0);
                //科室剩余预约数  -1表示无号
                bookingScheduleRuleVo.setAvailableNumber(-1);
            }
            // 当天有号 设置可预约日期
            bookingScheduleRuleVo.setWorkDate(date);
            bookingScheduleRuleVo.setWorkDateMd(date);

            // 计算当前预约日期为周几
            String dayOfWeek = this.getDayOfWeek(new DateTime(date));
            bookingScheduleRuleVo.setDayOfWeek(dayOfWeek);

            // 最后一页最后一条记录为即将预约   状态 0：正常 1：即将放号 -1：当天已停止挂号
            if (i == len - 1 && page == iPage.getPages()) {
                bookingScheduleRuleVo.setStatus(1);
            } else {
                bookingScheduleRuleVo.setStatus(0);
            }

            if (i == 0 && page == 1) {
                // 停止挂号时间
                DateTime stopTime = this.getDateTime(new Date(), bookingRule.getStopTime());
                // 如果当天预约时间过了停号时间， 不能预约
                if (stopTime.isBeforeNow()) {
                    //停止预约
                    bookingScheduleRuleVo.setStatus(-1);
                }
            }
            bookingScheduleRuleVoList.add(bookingScheduleRuleVo);
        }

        // 8. 放到map集合中返回
        Map<String, Object> result = new HashMap<>();
        // 可预约日期规则数据
        result.put("bookingScheduleList", bookingScheduleRuleVoList);
        result.put("total", iPage.getTotal());
        // 封装其他数据
        Map<String, String> baseMap = new HashMap<>();
        // 医院名称
        baseMap.put("hosname", hospitalService.getHospitalByHoscode(hoscode).getHosname());
        // 科室
        Department department = departmentService.getDepartment(hoscode, depcode);
        // 大科室名称
        baseMap.put("bigname", department.getBigname());
        // 科室名称
        baseMap.put("depname", department.getDepname());
        // 月
        baseMap.put("workDateString", new DateTime().toString("yyyy年MM月"));
        // 放号时间
        baseMap.put("releaseTime", bookingRule.getReleaseTime());
        // 停号时间
        baseMap.put("stopTime", bookingRule.getStopTime());
        result.put("baseMap", baseMap);
        return result;
    }

    /**
     * 7.获取排班详情-根据排班id
     * @param id 排班id
     * @return
     */
    @Override
    public Schedule getByScheduleId(String id) {
        Schedule schedule = this.packageSchedule(scheduleRepository.findById(id).get());
        return schedule;
    }

    /**
     * 8.获取预约下单数据-根据排班id
     * @param scheduleId
     * @return
     */
    @Override
    public ScheduleOrderVo getScheduleOrderVo(String scheduleId) {
        // 1.获取排班信息
        Schedule schedule = this.getByScheduleId(scheduleId);
        if(null == schedule) {
            throw new YyghException(20001,"排班数据为空");
        }

        // 2.获取医院的预约规则信息
        Hospital hospital = hospitalService.getHospitalByHoscode(schedule.getHoscode());
        if(null == hospital) {
            throw new YyghException(20001,"hospital数据为空");
        }
        BookingRule bookingRule = hospital.getBookingRule();
        if(null == bookingRule) {
            throw new YyghException(20001,"排版规则数据为空");
        }

        ScheduleOrderVo scheduleOrderVo = new ScheduleOrderVo();
        // 医院编号
        scheduleOrderVo.setHoscode(schedule.getHoscode());
        // 医院名称
        scheduleOrderVo.setHosname(hospital.getHosname());
        // 科室编号和名称
        scheduleOrderVo.setDepcode(schedule.getDepcode());
        scheduleOrderVo.setDepname(departmentService.getDepartment(schedule.getHoscode(), schedule.getDepcode()).getDepname());
        // 排班编号
        scheduleOrderVo.setHosScheduleId(schedule.getHosScheduleId());
        // 剩余号数量
        scheduleOrderVo.setAvailableNumber(schedule.getAvailableNumber());
        scheduleOrderVo.setTitle(schedule.getTitle());
        scheduleOrderVo.setReserveDate(schedule.getWorkDate());
        scheduleOrderVo.setReserveTime(schedule.getWorkTime());
        // 挂号费
        scheduleOrderVo.setAmount(schedule.getAmount());

        //退号截止天数（如：就诊前一天为-1，当天为0）
        int quitDay = bookingRule.getQuitDay();
        DateTime quitTime = this.getDateTime(new DateTime(schedule.getWorkDate()).plusDays(quitDay).toDate(), bookingRule.getQuitTime());
        scheduleOrderVo.setQuitTime(quitTime.toDate());

        //预约开始时间
        DateTime startTime = this.getDateTime(new Date(), bookingRule.getReleaseTime());
        scheduleOrderVo.setStartTime(startTime.toDate());

        //预约截止时间
        DateTime endTime = this.getDateTime(new DateTime().plusDays(bookingRule.getCycle()).toDate(), bookingRule.getStopTime());
        scheduleOrderVo.setEndTime(endTime.toDate());

        //当天停止挂号时间
        DateTime stopTime = this.getDateTime(new Date(), bookingRule.getStopTime());
        scheduleOrderVo.setStopTime(stopTime.toDate());
        return scheduleOrderVo;
    }

    /**
     * 更新排班
     * @param schedule 排班对象
     */
    @Override
    public void updateSchedule(Schedule schedule) {
        schedule.setUpdateTime(new Date());
        this.scheduleRepository.save(schedule);
    }

    /**
     * 获取可预约日期分页数据
     * @param page 当前页
     * @param limit 每页记录数
     * @param bookingRule 预约规则
     * @return
     */
    private IPage<Date> getListDate(int page, int limit, BookingRule bookingRule) {
        // 1.1获取预约周期
        int cycle = bookingRule.getCycle();
        // 1.2获取当天放号时间
        DateTime releaseTime = this.getDateTime(new Date(), bookingRule.getReleaseTime());

        // 2.当前日期 过了 放号日期 --> 预约周期+1
        if (releaseTime.isBeforeNow()) {
            cycle += 1;
        }

        // 3.获取所有可显示日期数据
        List<Date> dateList = new ArrayList<>();
        for (int i = 0; i < cycle; i++) {
            // 每天的日期+1
            DateTime curDateTime = new DateTime().plusDays(i);
            // 格式化为年月日
            String dateString = curDateTime.toString("yyyy-MM-dd");
            // 添加到list集合
            dateList.add(new DateTime(dateString).toDate());
        }

        // 4.日期分页，由于预约周期不一样，页面一排最多显示7天数据，多了就要分页显示
        List<Date> pageDateList = new ArrayList<>();
        // 开始页
        int start = (page - 1) * limit;
        // 结尾页
        int end = (page - 1) * limit + limit;
        if (end > dateList.size()) {
            end = dateList.size();
        }
        // 将要显示的日期放入pageDateList集合
        for (int i = start; i < end; i++) {
            pageDateList.add(dateList.get(i));
        }

        // 5.之前使用的是SpirngDate种的Page对象，这里使用mp中的Page对象
        IPage<Date> iPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page(page, 7, dateList.size());
        // 放入分页记录列表
        iPage.setRecords(pageDateList);

        return iPage;
    }

    /**
     * 将Date日期（yyyy-MM-dd HH:mm）转换为DateTime
     * @param date 当前日期
     * @param timeString 时间
     * @return DateTime(当前日期 + 时间)
     */
    private DateTime getDateTime(Date date, String timeString) {
        String dateTimeString = new DateTime(date).toString("yyyy-MM-dd") + " " + timeString;
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm").parseDateTime(dateTimeString);
        return dateTime;
    }

    @Autowired
    private DepartmentService departmentService;

    /**
     * 封装排班详情其他值 医院名称、科室名称、日期对应星期
     * @param schedule
     */
    private Schedule packageSchedule(Schedule schedule) {
        // 1. 设置医院名称
        schedule.getParam().put("hosname", hospitalService.getHospitalByHoscode(schedule.getHoscode()).getHosname());
        // 2. 设置科室名称
        schedule.getParam().put("depname",
                departmentService.getDepName(schedule.getHoscode(), schedule.getDepcode()));
        // 3. 设置日期对应星期
        schedule.getParam().put("dayOfWeek", this.getDayOfWeek(new DateTime(schedule.getWorkDate())));
        return schedule;
    }

    /**
     * 根据日期获取周几数据
     * @param dateTime org.joda.time工具类
     * @return
     */
    private String getDayOfWeek(DateTime dateTime) {
        String dayOfWeek = "";
        switch (dateTime.getDayOfWeek()) {
            case DateTimeConstants.SUNDAY:
                dayOfWeek = "周日";
                break;
            case DateTimeConstants.MONDAY:
                dayOfWeek = "周一";
                break;
            case DateTimeConstants.TUESDAY:
                dayOfWeek = "周二";
                break;
            case DateTimeConstants.WEDNESDAY:
                dayOfWeek = "周三";
                break;
            case DateTimeConstants.THURSDAY:
                dayOfWeek = "周四";
                break;
            case DateTimeConstants.FRIDAY:
                dayOfWeek = "周五";
                break;
            case DateTimeConstants.SATURDAY:
                dayOfWeek = "周六";
            default:
                break;
        }
        return dayOfWeek;
    }
}
