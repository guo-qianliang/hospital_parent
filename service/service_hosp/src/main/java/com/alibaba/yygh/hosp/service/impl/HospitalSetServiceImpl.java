package com.alibaba.yygh.hosp.service.impl;

import com.alibaba.yygh.common.exception.YyghException;
import com.alibaba.yygh.model.hosp.Hospital;
import com.alibaba.yygh.model.hosp.HospitalSet;
import com.alibaba.yygh.hosp.mapper.HospitalSetMapper;
import com.alibaba.yygh.hosp.service.HospitalSetService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医院设置表 服务实现类
 * </p>
 *
 * @author gql
 * @since 2021-10-31
 */
@Service
public class HospitalSetServiceImpl extends ServiceImpl<HospitalSetMapper, HospitalSet> implements HospitalSetService {
    /**
     * 根据医院编号hoscode查询signKey
     * @param hoscode
     * @return signKey
     */
    @Override
    public String getSignKey(String hoscode) {
        QueryWrapper<HospitalSet> wrapper = new QueryWrapper<>();
        wrapper.eq("hoscode", hoscode);
        HospitalSet hospitalSet = baseMapper.selectOne(wrapper);
        if (null == hospitalSet) {
            throw new YyghException(20001, "医院信息不存在");
        }
        return hospitalSet.getSignKey();
    }
}
