package com.alibaba.yygh.hosp.controller.test;

import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.hosp.testmongo.User;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * description: TestMongoTemplate
 * MongoTemplate做聚合方便
 * @author: gql
 * @date: 2021/11
 */
@Api(tags = "Test:测试MongoTemplate")
@RestController
@RequestMapping("/mongoTemplate")
public class TestMongoTemplate {

    @Autowired
    private MongoTemplate mongoTemplate;

    @ApiOperation("1.添加操作")
    @PostMapping("createUser")
    public R createUser() {
        User user = new User();
        user.setAge(20);
        user.setName("test");
        user.setEmail("hudie@qq.com");
        User insert = this.mongoTemplate.insert(user);
        return R.ok();
    }

    @ApiOperation("2.查询所有")
    @GetMapping("findAll")
    public R findUser() {
        List<User> userList = mongoTemplate.findAll(User.class);
        return R.ok().data("userList", userList);
    }

    @ApiOperation("3.根据id查询")
    @GetMapping("findId/{id}")
    public R getById(@PathVariable String id) {
        User user = mongoTemplate.findById(id, User.class);
        return R.ok().data("user", user);
    }

    // 使用Repository会更方便
    @ApiOperation("4.条件查询")
    @GetMapping("findUser")
    public R findUserList() {
        Query query = new Query(Criteria
                .where("name").is("test")
                .and("age").is(20));
        List<User> userList = mongoTemplate.find(query, User.class);
        return R.ok().data("userList", userList);
    }

    @ApiOperation("5.模糊查询")
    @GetMapping("findLike")
    public R findUsersLikeName() {
        // 设置模糊匹配规则
        String name = "est";
        String regex = String.format("%s%s%s", "^.*", name, ".*$");
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        // 调用方法
        Query query = new Query(Criteria.where("name").regex(pattern));
        List<User> userList = mongoTemplate.find(query, User.class);
        return R.ok().data("userList", userList);
    }

    @ApiOperation("6.分页查询")
    @GetMapping("findPage")
    public R findUsersPage() {
        String name = "est";
        int pageNo = 1;
        int pageSize = 10;
        // 封装条件
        Query query = new Query();
        String regex = String.format("%s%s%s", "^.*", name, ".*$");
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        query.addCriteria(Criteria.where("name").regex(pattern));
        // 分页 limit (当前页-1) * 每页记录数,
        int totalCount = (int) mongoTemplate.count(query, User.class);
        List<User> userList = mongoTemplate.find(query.skip((pageNo - 1) * pageSize)
                .limit(pageSize), User.class);
        Map<String, Object> pageMap = new HashMap<>();
        pageMap.put("list", userList);
        pageMap.put("totalCount", totalCount);
        return R.ok().data("pageMap",pageMap);
    }

    @ApiOperation("7.修改")
    @PutMapping("update/{id}")
    public R updateUser(@PathVariable String id) {
        // 根据id查询
        User user = mongoTemplate.findById(id, User.class);
        // 设置要修改的值
        user.setName("test_1");
        user.setAge(25);
        user.setEmail("huahua@qq.com");
        // 设置mongoTemplate需要的数据
        // 设置id部分
        Query query = new Query(Criteria.where("_id").is(user.getId()));
        // 设置id意外的其他部分
        Update update = new Update();
        update.set("name", user.getName());
        update.set("age", user.getAge());
        update.set("email", user.getEmail());
        // 调用修改方法
        UpdateResult result = mongoTemplate.upsert(query, update, User.class);
        long count = result.getModifiedCount();
        return R.ok().data("count",count);
    }

    @ApiOperation("8.删除操作")
    @DeleteMapping("delete/{id}")
    public R delete(@PathVariable String id) {
        Query query =
                new Query(Criteria.where("_id").is(id));
        DeleteResult result = mongoTemplate.remove(query, User.class);
        long count = result.getDeletedCount();
        return R.ok().data("count",count);
    }

}
