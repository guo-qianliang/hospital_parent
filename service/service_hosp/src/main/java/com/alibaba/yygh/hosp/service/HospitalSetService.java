package com.alibaba.yygh.hosp.service;

import com.alibaba.yygh.model.hosp.HospitalSet;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 医院设置表 服务类
 * </p>
 *
 * @author gql
 * @since 2021-10-31
 */
public interface HospitalSetService extends IService<HospitalSet> {
    /**
     * 根据医院编号hoscode查询signKey
     * @param hoscode
     * @return
     */
    String getSignKey(String hoscode);
}
