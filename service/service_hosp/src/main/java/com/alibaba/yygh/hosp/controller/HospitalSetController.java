package com.alibaba.yygh.hosp.controller;


import com.alibaba.yygh.common.exception.YyghException;
import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.hosp.service.HospitalSetService;
import com.alibaba.yygh.model.hosp.HospitalSet;
import com.alibaba.yygh.vo.hosp.HospitalSetQueryVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * <p>
 * 医院设置表 前端控制器
 * </p>
 *
 * @author gql
 * @since 2021-10-31
 */
@Api(tags = "2.医院设置管理")
@RestController
@RequestMapping("/admin/hosp/hospitalSet")
//@CrossOrigin
public class HospitalSetController {
    @Autowired
    private HospitalSetService hospitalSetService;

    @ApiOperation("1.查询医院设置列表")
    @GetMapping("findAll")
    public R findAll() {
        List<HospitalSet> list = this.hospitalSetService.list();
        return R.ok().data("rows", list);
    }

    @ApiOperation("2.查询医院设置列表-带分页")
    @GetMapping("{currentPage}/{limit}")
    public R findPage(
            @PathVariable Long currentPage,
            @PathVariable Long limit) {
        // 分页对象
        Page<HospitalSet> page = new Page<>(currentPage, limit);
        // 封装分页数据到page
        hospitalSetService.page(page, null);
        // 返回数据
        long total = page.getTotal();
        List<HospitalSet> records = page.getRecords();
        return R.ok().data("total", total).data("rows", records);
    }

    @ApiOperation("3.查询医院设置列表-条件查询带分页√")
    @PostMapping("findPageQuery/{currentPage}/{limit}")
    public R findPageQuery(
            // 当前页
            @PathVariable Long currentPage,
            // 每页记录数
            @PathVariable Long limit,
            // 查询条件
            @RequestBody(required = false) HospitalSetQueryVo hospitalSetQueryVo) {
        // 1.创建分页对象page
        Page<HospitalSet> page = new Page<>(currentPage, limit);

        QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
        // 2.封装查询条件到page对象
        if (null == hospitalSetQueryVo) {
            this.hospitalSetService.page(page, queryWrapper);
        } else {
            String hosname = hospitalSetQueryVo.getHosname();
            String hoscode = hospitalSetQueryVo.getHoscode();
            if (!StringUtils.isEmpty(hosname)) {
                queryWrapper.like("hosname", hosname);
            }
            if (!StringUtils.isEmpty(hoscode)) {
                queryWrapper.eq("hoscode", hoscode);
            }
            this.hospitalSetService.page(page, queryWrapper);
        }

        // 获取总记录数total
        long total = page.getTotal();
        // 获取当前页数据集合
        List<HospitalSet> records = page.getRecords();
        // 3.返回数据
        return R.ok().data("total", total).data("rows", records);
    }

    @ApiOperation("4.删除医院设置信息-id")
    @DeleteMapping("remove/{id}")
    public R removeById(@PathVariable String id) {
        boolean flag = this.hospitalSetService.removeById(id);
        if (flag) {
            return R.ok().message("删除成功");
        } else {
            return R.error().message("删除失败");
        }
    }

    @ApiOperation("5.添加医院设置")
    @PostMapping("saveHospSet")
    public R save(
            @ApiParam(name = "hospitalSet", value = "医院设置对象", required = true)
            @RequestBody HospitalSet hospitalSet) {

        //设置状态 1 使用 0 不能使用
        hospitalSet.setStatus(1);
        //签名秘钥
        Random random = new Random();
        //hospitalSet.setSignKey(MD5.encrypt(System.currentTimeMillis() + "" + random.nextInt(1000)));

        boolean flag = hospitalSetService.save(hospitalSet);
        if (flag) {
            return R.ok().message("添加成功");
        } else {
            return R.error().message("添加失败");
        }
    }

    @ApiOperation("6-1 根据ID查询医院设置")
    @GetMapping("getHospSet/{id}")
    public R getHospSetById(@PathVariable String id) {
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        return R.ok().data("hospitalSet", hospitalSet);
    }

    @ApiOperation("6-2 根据ID修改医院设置")
    @PostMapping("updateHospSet")
    public R updateById(@RequestBody HospitalSet hospitalSet) {
        boolean flag = hospitalSetService.updateById(hospitalSet);
        if (flag) {
            return R.ok().message("修改成功");
        } else {
            return R.error().message("修改失败");
        }
    }


    @ApiOperation("7 删除医院设置-批量删除")
    @DeleteMapping("batchRemove")
    public R batchRemoveHospitalSet(@RequestBody List<Long> idList) {
        boolean flag = hospitalSetService.removeByIds(idList);
        if (flag) {
            return R.ok().message("删除成功");
        } else {
            return R.error().message("删除失败");
        }
    }

    @ApiOperation("8 医院设置锁定和解锁-更新status")
    @PutMapping("lockHospitalSet/{id}/{status}")
    public R lockHospitalSet(@PathVariable Long id,
                             @PathVariable Integer status) {
        //根据id查询医院设置信息
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        //设置状态
        hospitalSet.setStatus(status);
        //调用方法
        hospitalSetService.updateById(hospitalSet);
        return R.ok();
    }

    @ApiOperation("a. 模拟异常")
    @GetMapping("testException")
    public R testException() {
        // 为了测试,模拟异常
        try {
            int i = 10 / 0;
        } catch (Exception e) {
            // 抛出自定义异常
            throw new YyghException(20001, "执行自定义异常处理");
        }
        return R.ok().data("msg", "模拟异常");
    }
}

