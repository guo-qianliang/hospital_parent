package com.alibaba.yygh.hosp.controller.test;

import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.hosp.testmongo.User;
import com.alibaba.yygh.hosp.testmongo.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * description: TestMongoRepository
 * MongoRepository查询方便
 * @author: gql
 * @date: 2021/11
 */
@Api(tags = "Test:测试MongoRepository")
@RestController
@RequestMapping("/mongoRepository")
public class TestMongoRepository {
    @Autowired
    private UserRepository userRepository;

    @ApiOperation("1.添加")
    @PostMapping("create")
    public R createUser() {
        User user = new User();
        user.setAge(20);
        user.setName("张三");
        user.setEmail("zs@qq.com");
        // save可以做添加和修改,由是否有id值决定
        User user1 = userRepository.save(user);
        return R.ok();
    }

    @ApiOperation("2.查询所有")
    @GetMapping("findAll")
    public R findUser() {
        List<User> userList = userRepository.findAll();
        return R.ok().data("userList", userList);
    }

    @ApiOperation("3.根据id查询")
    @GetMapping("findId/{id}")
    public R getById(@PathVariable String id) {
        User user = userRepository.findById(id).get();
        return R.ok().data("user", user);
    }

    @ApiOperation("4.条件查询")
    @GetMapping("findQuery")
    public R findUserList() {
        User user = new User();
        user.setName("张三");
        user.setAge(20);
        Example<User> userExample = Example.of(user);
        List<User> userList = userRepository.findAll(userExample);
        return R.ok().data("userList", userList);
    }

    @ApiOperation("5.模糊查询")
    @GetMapping("findLike")
    public R findUsersLikeName() {
        //创建匹配器，即如何使用查询条件
        ExampleMatcher matcher = ExampleMatcher.matching() //构建对象
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING) //改变默认字符串匹配方式：模糊查询
                .withIgnoreCase(true); //改变默认大小写忽略方式：忽略大小写
        User user = new User();
        user.setName("三");
        Example<User> userExample = Example.of(user, matcher);
        List<User> userList = userRepository.findAll(userExample);
        return R.ok().data("userList", userList);
    }

    @ApiOperation("6.分页查询")
    @GetMapping("findPage")
    public R findUsersPage() {
        // 设置排序
        Sort sort = Sort.by(Sort.Direction.DESC, "age");
        // 设置分页
        /**
         * args1:当前页
         * args2:每页显示记录数
         * args3:排序规则
         */
        Pageable pageable = PageRequest.of(0, 10, sort);
        // 创建条件匹配器,实现模糊查询
        ExampleMatcher matcher = ExampleMatcher.matching() //构建对象
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING) //改变默认字符串匹配方式：模糊查询
                .withIgnoreCase(true); //改变默认大小写忽略方式：忽略大小写
        User user = new User();
        user.setName("三");
        Example<User> userExample = Example.of(user, matcher);
        // 封装条件
        Example<User> example = Example.of(user, matcher);
        // 调用方法
        Page<User> pages = userRepository.findAll(example, pageable);
        return R.ok().data("pages", pages);
    }

    @ApiOperation("7.修改")
    @PutMapping("update/{id}")
    public R updateUser(@PathVariable String id) {
        User user = userRepository.findById(id).get();
        user.setName("张三_1");
        user.setAge(25);
        user.setEmail("张三_1@qq.com");
        User save = userRepository.save(user);
        return R.ok().data("save", save);
    }

    @ApiOperation("8.删除")
    @DeleteMapping("delete/{id}")
    public R delete(@PathVariable String id) {
        userRepository.deleteById(id);
        return R.ok();
    }

    @ApiOperation("测试SpringData方法规范")
    public R findByNameLike() {
        List<User> users = userRepository.findByNameLike("张");
        return R.ok().data("users", users);
    }

    @ApiOperation("测试SpringData方法规范")
    @GetMapping("testMethod1")
    public R findByName() {
        List<User> users = userRepository.findByName("张三");
        return R.ok().data("users", users);
    }
}
