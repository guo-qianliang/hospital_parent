package com.alibaba.yygh.hosp.service;

import com.alibaba.yygh.model.hosp.Schedule;
import com.alibaba.yygh.vo.hosp.ScheduleOrderVo;
import com.alibaba.yygh.vo.hosp.ScheduleQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * description: ScheduleService
 * @author: gql
 * @date: 2021/11
 */
public interface ScheduleService {
    /**
     * 1.上传排班
     * @param paramMap
     */
    void save(Map<String, Object> paramMap);

    /**
     * 2.查询排班列表-带分页
     * @param page 当前页
     * @param limit 每页记录数
     * @param scheduleQueryVo 查询条件
     * @return
     */
    Page<Schedule> selectPage(int page, int limit, ScheduleQueryVo scheduleQueryVo);

    /**
     * 3.删除排班
     * @param hoscode 医院编码
     * @param hosScheduleId 医院排班id
     */
    void remove(String hoscode, String hosScheduleId);

    /**
     * 1.查询排班规则数据-带分页
     * @param page 当前页
     * @param limit 每页记录数
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @return
     */
    Map<String, Object> getRuleSchedule(long page, long limit, String hoscode, String depcode);

    /**
     * 2.查询每日排班详细信息
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @param workDate 工作日期
     * @return
     */
    List<Schedule> getDetailSchedule(String hoscode, String depcode, String workDate);

    /**
     * 5.获取可预约排班数据
     * @param page 当前页
     * @param limit 每页记录数
     * @param hoscode 医院编号
     * @param depcode 科室编号
     * @return
     */
    Map<String, Object> getBookingScheduleRule(Integer page, Integer limit, String hoscode, String depcode);

    /**
     * 7.获取排班详情-根据排班id
     * @param id
     * @return
     */
    Schedule getByScheduleId(String id);

    /**
     * 8.获取预约下单数据-根据排班id
     * @param scheduleId
     * @return
     */
    ScheduleOrderVo getScheduleOrderVo(String scheduleId);

    /**
     * 更新排班
     * @param schedule 排班对象
     */
    void updateSchedule(Schedule schedule);
}
