package com.alibaba.yygh.hosp.service;

import com.alibaba.yygh.model.hosp.Hospital;
import com.alibaba.yygh.vo.hosp.HospitalQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * description: HospitalService
 * @author: gql
 * @date: 2021/11
 */
public interface HospitalService {
    /**
     * 1.上传医院到MongoDB
     * @param paramMap
     */
    void saveHosp(Map<String, Object> paramMap);

    /**
     * 2.根据hoscode获取医院信息
     * @param hoscode
     * @return
     */
    Hospital getHospitalByHoscode(String hoscode);

    /**
     * 3.查询医院列表-带分页
     * @param page 当前页
     * @param limit 每页记录数
     * @param hospitalQueryVo 查询条件
     * @return
     */
    Page<Hospital> selectPage(Integer page, Integer limit, HospitalQueryVo hospitalQueryVo);

    /**
     * 4.查询医院详情
     * @param id
     * @return
     */
    Map<String, Object> show(String id);

    /**
     * 2.根据医院名称获取医院列表
     * @param hosname
     * @return
     */
    List<Hospital> findByHosname(String hosname);

    /**
     * 4.获取医院预约挂号详情
     * @param hoscode
     * @return
     */
    Map<String, Object> getHospDetail(String hoscode);
}
