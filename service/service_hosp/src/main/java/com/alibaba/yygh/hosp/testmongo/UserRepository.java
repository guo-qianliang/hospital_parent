package com.alibaba.yygh.hosp.testmongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * description: UserRepository
 * @author: gql
 * @date: 2021/11
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {
    /**
     * 根据name查询
     * @param name
     * @return
     */
    List<User> findByName(String name);

    /**
     * 根据name模糊查询
     * @param name
     * @return
     */
    List<User> findByNameLike(String name);
}
