package com.alibaba.yygh.hosp.controller.api;

import com.alibaba.yygh.common.exception.YyghException;
import com.alibaba.yygh.common.result.Result;
import com.alibaba.yygh.hosp.service.DepartmentService;
import com.alibaba.yygh.hosp.service.HospitalService;
import com.alibaba.yygh.hosp.service.HospitalSetService;
import com.alibaba.yygh.hosp.service.ScheduleService;
import com.alibaba.yygh.hosp.utils.HttpRequestHelper;
import com.alibaba.yygh.hosp.utils.MD5;
import com.alibaba.yygh.model.hosp.Department;
import com.alibaba.yygh.model.hosp.Hospital;
import com.alibaba.yygh.model.hosp.Schedule;
import com.alibaba.yygh.vo.hosp.DepartmentQueryVo;
import com.alibaba.yygh.vo.hosp.ScheduleQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * description: ApiController 医院管理Controller，此处接口需被外界调用
 * @author: gql
 * @date: 2021/11
 */
@Api(tags = "1.医院管理API接口")
@RestController
@RequestMapping("/api/hosp")
public class ApiController {
    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private HospitalSetService hospitalSetService;

    //========================1.医院接口========================
    @ApiOperation(value = "1.上传医院到MongoDB")
    @PostMapping("saveHospital")
    public Result saveHosp(HttpServletRequest request) {
        // 1.医院信息JSON格式转换为Object类型
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        // 2.获取医院管理表中的密钥(已经使用MD5加密过了)
        String hospSignKeyMD5 = (String) paramMap.get("sign");

        // 3.获取医院设置表中的密钥并进行MD5加密
        String hoscode = (String) paramMap.get("hoscode");
        if (StringUtils.isEmpty(hoscode)) {
            throw new YyghException(20001, "参数错误");
        }
        String yyghSignKey = hospitalSetService.getSignKey(hoscode);
        String yyghSignKeyMd5 = MD5.encrypt(yyghSignKey);

        // 4.密钥不匹配就抛出错误
        if (!hospSignKeyMD5.equals(yyghSignKeyMd5)) {
            throw new YyghException(20001, "签名校验失败!");
        }

        // 5.传递的图片涉及base64编码问题，需要将logoData记录中所有的" "替换为+
        String logoData = (String) paramMap.get("logoData");
        logoData = logoData.replaceAll(" ", "+");
        paramMap.put("logoData", logoData);

        // 6.执行上传操作
        hospitalService.saveHosp(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "2.获取医院信息")
    @PostMapping("hospital/show")
    public Result getHospital(HttpServletRequest request) {
        // 1.医院信息JSON格式转换为Object类型
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        // 2.获取医院管理表中的密钥(已经使用MD5加密过了)
        String hospSignKeyMD5 = (String) paramMap.get("sign");

        // 3.获取医院设置表中的密钥并进行MD5加密
        String hoscode = (String) paramMap.get("hoscode");
        if (StringUtils.isEmpty(hoscode)) {
            throw new YyghException(20001, "hoscode参数为空!");
        }
        String yyghSignKey = hospitalSetService.getSignKey(hoscode);
        String yyghSignKeyMd5 = MD5.encrypt(yyghSignKey);

        // 4.密钥不匹配就抛出错误
        if (!hospSignKeyMD5.equals(yyghSignKeyMd5)) {
            throw new YyghException(20001, "签名校验失败");
        }

        // 5.执行查询操作
        Hospital hospital = hospitalService.getHospitalByHoscode(hoscode);

        return Result.ok(hospital);
    }

    //========================2.科室接口========================

    @Autowired
    private DepartmentService departmentService;

    @ApiOperation(value = "1.上传科室信息")
    @PostMapping("saveDepartment")
    public Result saveDepartment(HttpServletRequest request) {
        // 1.将传递过来的数组类型转换为Object类型
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        // 2.获取医院管理表中的密钥(已经使用MD5加密好了)
        String hospSignKeyMD5 = (String) paramMap.get("sign");

        // 3.获取医院设置表中的密钥并进行MD5加密
        String hoscode = (String) paramMap.get("hoscode");
        if (StringUtils.isEmpty(hoscode)) {
            throw new YyghException(20001, "hoscode参数为空!");
        }
        String yyghSignKey = hospitalSetService.getSignKey(hoscode);
        String yyghSignKeyMd5 = MD5.encrypt(yyghSignKey);

        // 4.密钥不匹配就抛出错误
        if (!hospSignKeyMD5.equals(yyghSignKeyMd5)) {
            throw new YyghException(20001, "签名校验失败");
        }

        // 5.执行上传科室操作
        departmentService.save(paramMap);
        return Result.ok();
    }


    @ApiOperation(value = "2.查询科室信息列表-带分页")
    @PostMapping("department/list")
    public Result findDepartment(HttpServletRequest request) {
        // 1.将传递过来的数组类型转换为Object类型
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        // 2.获取医院管理表中的密钥(已经使用MD5加密好了)
        String hospSignKeyMD5 = (String) paramMap.get("sign");

        // 3.获取医院设置表中的密钥并进行MD5加密
        String hoscode = (String) paramMap.get("hoscode");
        if (StringUtils.isEmpty(hoscode)) {
            throw new YyghException(20001, "hoscode参数为空!");
        }
        String yyghSignKey = hospitalSetService.getSignKey(hoscode);
        String yyghSignKeyMd5 = MD5.encrypt(yyghSignKey);

        // 4.比较密钥
        if (!hospSignKeyMD5.equals(yyghSignKeyMd5)) {
            throw new YyghException(20001, "签名校验失败");
        }

        /**
         * 5.准备分页数据信息
         * departmentQueryVo 查询条件
         * page:当前页
         * limit:每页记录数
         */
        DepartmentQueryVo departmentQueryVo = new DepartmentQueryVo();
        String depcode = (String) paramMap.get("depcode"); // 科室编号
        departmentQueryVo.setHoscode(depcode);
        departmentQueryVo.setHoscode(hoscode);
        int page = StringUtils.isEmpty(paramMap.get("page")) ? 1 : Integer.parseInt((String) paramMap.get("page"));
        int limit = StringUtils.isEmpty(paramMap.get("limit")) ? 1 : Integer.parseInt((String) paramMap.get("limit"));

        // 6. 执行查询科室操作
        Page<Department> pages = departmentService.findPageDepartment(page, limit, departmentQueryVo);
        return Result.ok(pages);
    }

    @ApiOperation(value = "3.删除科室")
    @PostMapping("department/remove")
    public Result removeDepartment(HttpServletRequest request) {
        // 1.将传递过来的数组类型转换为Object类型
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        // 2.获取医院管理表中的密钥(已经使用MD5加密好了)
        String hospSignKeyMD5 = (String) paramMap.get("sign");

        // 3.获取医院设置表中的密钥并进行MD5加密
        String hoscode = (String) paramMap.get("hoscode");
        if (StringUtils.isEmpty(hoscode)) {
            throw new YyghException(20001, "hoscode参数为空!");
        }
        String yyghSignKey = hospitalSetService.getSignKey(hoscode);
        String yyghSignKeyMd5 = MD5.encrypt(yyghSignKey);

        // 4.比较密钥
        if (!hospSignKeyMD5.equals(yyghSignKeyMd5)) {
            throw new YyghException(20001, "签名校验失败");
        }

        String depcode = (String) paramMap.get("depcode");
        // 5.根据hoscode和depcode删除科室信息
        departmentService.remove(hoscode, depcode);
        return Result.ok();
    }

    //========================3.排班接口========================
    @Autowired
    private ScheduleService scheduleService;

    @ApiOperation(value = "1.上传排班")
    @PostMapping("saveSchedule")
    public Result saveSchedule(HttpServletRequest request) {
        // 1.将传递过来的数组类型转换为Object类型
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        // 2.获取医院管理表中的密钥(已经使用MD5加密好了)
        String hospSignKeyMD5 = (String) paramMap.get("sign");

        // 3.获取医院设置表中的密钥并进行MD5加密
        String hoscode = (String) paramMap.get("hoscode");
        if (StringUtils.isEmpty(hoscode)) {
            throw new YyghException(20001, "hoscode参数为空!");
        }
        String yyghSignKey = hospitalSetService.getSignKey(hoscode);
        String yyghSignKeyMd5 = MD5.encrypt(yyghSignKey);

        // 4.比较密钥
        if (!hospSignKeyMD5.equals(yyghSignKeyMd5)) {
            throw new YyghException(20001, "签名校验失败");
        }

        // 5.上传排班信息
        scheduleService.save(paramMap);
        return Result.ok();
    }

    @ApiOperation(value = "2.查询排班列表-带分页")
    @PostMapping("schedule/list")
    public Result schedule(HttpServletRequest request) {
        // 1.将传递过来的数组类型转换为Object类型
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        // 2.获取医院管理表中的密钥(已经使用MD5加密好了)
        String hospSignKeyMD5 = (String) paramMap.get("sign");

        // 3.获取医院设置表中的密钥并进行MD5加密
        String hoscode = (String) paramMap.get("hoscode");
        if (StringUtils.isEmpty(hoscode)) {
            throw new YyghException(20001, "hoscode参数为空!");
        }
        String yyghSignKey = hospitalSetService.getSignKey(hoscode);
        String yyghSignKeyMd5 = MD5.encrypt(yyghSignKey);

        // 4.比较密钥
        if (!hospSignKeyMD5.equals(yyghSignKeyMd5)) {
            throw new YyghException(20001, "签名校验失败");
        }

        /**
         * 5.准备分页数据信息
         * departmentQueryVo 查询条件
         * page:当前页
         * limit:每页记录数
         */
        ScheduleQueryVo scheduleQueryVo = new ScheduleQueryVo();
        String depcode = (String)paramMap.get("depcode");// 科室编号
        scheduleQueryVo.setDepcode(depcode);
        scheduleQueryVo.setHoscode(hoscode);
        int page = StringUtils.isEmpty(paramMap.get("page")) ? 1 : Integer.parseInt((String)paramMap.get("page"));
        int limit = StringUtils.isEmpty(paramMap.get("limit")) ? 10 : Integer.parseInt((String)paramMap.get("limit"));

        // 6.执行查询排班操作
        Page<Schedule> pageModel = scheduleService.selectPage(page , limit, scheduleQueryVo);
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "3.删除排班")
    @PostMapping("schedule/remove")
    public Result removeSchedule(HttpServletRequest request) {
        // 1.将传递过来的数组类型转换为Object类型
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(request.getParameterMap());

        // 2.获取医院管理表中的密钥(已经使用MD5加密好了)
        String hospSignKeyMD5 = (String) paramMap.get("sign");

        // 3.获取医院设置表中的密钥并进行MD5加密
        String hoscode = (String) paramMap.get("hoscode");
        if (StringUtils.isEmpty(hoscode)) {
            throw new YyghException(20001, "hoscode参数为空!");
        }
        String yyghSignKey = hospitalSetService.getSignKey(hoscode);
        String yyghSignKeyMd5 = MD5.encrypt(yyghSignKey);

        // 4.比较密钥
        if (!hospSignKeyMD5.equals(yyghSignKeyMd5)) {
            throw new YyghException(20001, "签名校验失败");
        }

        // 5.执行删除排班操作
        String hosScheduleId = (String)paramMap.get("hosScheduleId");
        scheduleService.remove(hoscode, hosScheduleId);
        return Result.ok();
    }

}
