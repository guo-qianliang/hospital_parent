package com.alibaba.yygh.hosp.controller;

import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.hosp.service.ScheduleService;
import com.alibaba.yygh.model.hosp.Schedule;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * description: ScheduleController
 * @author: gql
 * @date: 2021/11
 */
@RestController
@RequestMapping("/admin/hosp/schedule")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @ApiOperation(value ="1.查询排班规则数据-带分页")
    @GetMapping("getScheduleRule/{page}/{limit}/{hoscode}/{depcode}")
    public R getScheduleRule(@PathVariable("page") long page,
                             @PathVariable("limit") long limit,
                             @PathVariable("hoscode") String hoscode,
                             @PathVariable("depcode") String depcode) {
        Map<String,Object> map
                = scheduleService.getRuleSchedule(page,limit,hoscode,depcode);
        return R.ok().data(map);
    }

    @ApiOperation(value = "2.查询每日排班详细信息")
    @GetMapping("getScheduleDetail/{hoscode}/{depcode}/{workDate}")
    public R getScheduleDetail( @PathVariable("hoscode") String hoscode,
                                @PathVariable("depcode") String depcode,
                                @PathVariable("workDate") String workDate) {
        List<Schedule> list = scheduleService.getDetailSchedule(hoscode,depcode,workDate);
        return R.ok().data("list",list);
    }
}
