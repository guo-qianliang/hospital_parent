package com.alibaba.yygh.hosp.repository;

import com.alibaba.yygh.model.hosp.Department;
import com.alibaba.yygh.vo.hosp.DepartmentVo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * description: DepartmentRepository
 * @author: gql
 * @date: 2021/11
 */
@Repository
public interface DepartmentRepository extends MongoRepository<Department,String> {
    /**
     * 根据hoscode和depcode查询医院科室信息
     * @param hoscode 医院编号
     * @param depcode 部门编号
     * @return
     */
    Department findDepartmentByHoscodeAndDepcode(String hoscode, String depcode);

    /**
     * 根据医院编号查询医院科室列表
     * @param hoscode 医院编号
     * @return
     */
    List<Department> findDepartmentByHoscode(String hoscode);


}
