package com.alibaba.yygh.hosp.controller;


import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.hosp.service.HospitalSetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 登录接口
 * 参考mock接口,创建本地接口返回相同数据.
 * </p>
 *
 * @author gql
 * @since 2021-11-3
 */
@Api(tags = "1.登录接口")
@RestController
@RequestMapping("/admin/hosp")
//@CrossOrigin
public class UserLoginController {
    @Autowired
    private HospitalSetService hospitalSetService;

    /**
     * http://localhost:9528/dev-api/vue-admin-template/user/login
     * {"code":20000,"data":{"token":"admin-token"}}
     * @return
     */
    @ApiOperation("1.登录")
    @PostMapping("user/login")
    public R login() {
        return R.ok().data("token", "admin-token");
    }

    /**
     * http://localhost:9528/dev-api/vue-admin-template/user/info?token=admin-token
     * {"code":20000,"data":{"roles":["admin"],"introduction":"I am a super administrator",
     * "avatar":"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
     * "name":"Super Admin"}}
     */
    @ApiOperation("2.获取信息")
    @GetMapping("user/info")
    public R info() {
        return R.ok().data("roles", "admin")
                .data("introduction", "I am a super administrator")
                // 头像
                .data("avatar", "https://hospital-yygh.oss-cn-beijing.aliyuncs.com/%E5%B0%8F%E8%93%9D%E5%A4%B4.jpg")
                .data("name", "超级管理员");
    }

}

