package com.alibaba.yygh.hosp.service;

import com.alibaba.yygh.model.hosp.Department;
import com.alibaba.yygh.vo.hosp.DepartmentQueryVo;
import com.alibaba.yygh.vo.hosp.DepartmentVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * description: DepartmentService
 * @author: gql
 * @date: 2021/11
 */
public interface DepartmentService {
    /**
     * 1.上传科室信息
     * @param paramMap 科室信息map
     */
    void save(Map<String, Object> paramMap);


    /**
     * 2.查询科室信息
     * @param page 当前页
     * @param limit 每页记录数
     * @param departmentQueryVo 查询条件
     * @return
     */
    Page<Department> findPageDepartment(int page, int limit, DepartmentQueryVo departmentQueryVo);

    /**
     * 3.删除科室
     * @param hoscode 医院编号
     * @param depcode 科室编号
     */
    void remove(String hoscode, String depcode);

    /**
     * 1.查询医院所有科室列表
     * @param hoscode 医院编号
     * @return List<DepartmentVo>
     */
    List<DepartmentVo> findDeptTree(String hoscode);

    /**
     * 2.根据医院编号、部门编号查询部门名称
     * @param hoscode 医院编号
     * @param depcode 部门编号
     * @return
     */
    String getDepName(String hoscode, String depcode);

    /**
     * 获取科室
     * @param hoscode
     * @param depcode
     * @return
     */
    Department getDepartment(String hoscode, String depcode);
}
