package com.alibaba.yygh.hosp.controller;

import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.hosp.service.HospitalService;
import com.alibaba.yygh.model.hosp.Hospital;
import com.alibaba.yygh.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * description: HospitalController
 * @author: gql
 * @date: 2021/11
 */
@Api(description = "1.医院接口")
@RestController
@RequestMapping("/admin/hosp/hospital")
//@CrossOrigin
public class HospitalController {

    @Autowired
    private HospitalService hospitalService;

    @ApiOperation(value = "1.查询医院列表-带分页")
    @GetMapping("findPageHosp/{page}/{limit}")
    public R index(@PathVariable("page") Integer page,
                   @PathVariable("limit") Integer limit,
                   HospitalQueryVo hospitalQueryVo) {
        Page<Hospital> pages = hospitalService.selectPage(page, limit, hospitalQueryVo);
        return R.ok().data("pages",pages);
    }

    @ApiOperation(value = "2.查询医院详情")
    @GetMapping("show/{id}")
    public R show(@PathVariable("id") String id) {
        /**
         * 之所以返回Map<String, Object>是因为vue的插值表达式不支持多级取值
         * 而Hospital中包含预约规则BookingRule对象
         */
        Map<String, Object> map = hospitalService.show(id);
        return R.ok().data(map);
    }
}
