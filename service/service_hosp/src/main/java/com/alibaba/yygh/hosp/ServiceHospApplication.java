package com.alibaba.yygh.hosp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * description: ServiceHospApplication 医院设置启动类
 * @author: gql
 * @date: 2021/10
 */

@SpringBootApplication
// 注册到Nacos
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.alibaba.yygh"})
/**
 * basePackages：加载依赖的其他模块的配置类
 */
@ComponentScan(basePackages = {"com.alibaba.yygh"})
public class ServiceHospApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceHospApplication.class, args);
    }
}
