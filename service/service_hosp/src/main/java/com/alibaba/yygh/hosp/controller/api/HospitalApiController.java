package com.alibaba.yygh.hosp.controller.api;

import com.alibaba.yygh.common.result.R;
import com.alibaba.yygh.hosp.service.DepartmentService;
import com.alibaba.yygh.hosp.service.HospitalService;
import com.alibaba.yygh.hosp.service.ScheduleService;
import com.alibaba.yygh.model.hosp.Hospital;
import com.alibaba.yygh.model.hosp.Schedule;
import com.alibaba.yygh.vo.hosp.DepartmentVo;
import com.alibaba.yygh.vo.hosp.HospitalQueryVo;
import com.alibaba.yygh.vo.hosp.ScheduleOrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * description: HospitalApiController
 * @author: gql
 * @date: 2021/11
 */
@Api(tags = "1.医院显示接口")
@RestController
@RequestMapping("/api/hosp/hospital")
public class HospitalApiController {

    @Autowired
    private HospitalService hospitalService;

    @ApiOperation(value = "1.获取分页列表")
    @GetMapping("{page}/{limit}")
    public R index(
            @PathVariable("page") Integer page,
            @PathVariable("limit") Integer limit,
            HospitalQueryVo hospitalQueryVo) {

        Page<Hospital> pageModel = hospitalService.selectPage(page, limit, hospitalQueryVo);
        return R.ok().data("pages", pageModel);
    }

    @ApiOperation(value = "2.根据医院名称获取医院列表-模糊查询")
    @GetMapping("findByHosname/{hosname}")
    public R findByHosname(
            @PathVariable(value = "hosname") String hosname) {

        List<Hospital> list = hospitalService.findByHosname(hosname);
        return R.ok().data("list", list);
    }

    @Autowired
    private DepartmentService departmentService;

    @ApiOperation(value = "3.获取科室列表")
    @GetMapping("department/{hoscode}")
    public R index(
            @PathVariable("hoscode") String hoscode) {
        List<DepartmentVo> list = departmentService.findDeptTree(hoscode);
        return R.ok().data("list", list);
    }

    @ApiOperation(value = "4.获取医院预约挂号详情")
    @GetMapping("{hoscode}")
    public R item(
            @PathVariable("hoscode") String hoscode) {
        Map<String, Object> map = hospitalService.getHospDetail(hoscode);
        return R.ok().data(map);
    }

    @Autowired
    private ScheduleService scheduleService;

    @ApiOperation(value = "5.获取可预约排班数据")
    @GetMapping("auth/getBookingScheduleRule/{page}/{limit}/{hoscode}/{depcode}")
    public R getBookingSchedule(
            @PathVariable Integer page,
            @PathVariable Integer limit,
            @PathVariable String hoscode,
            @PathVariable String depcode) {

        Map<String, Object> map = scheduleService.getBookingScheduleRule(page, limit, hoscode, depcode);
        return R.ok().data(map);
    }

    @ApiOperation(value = "6.获取排班数据")
    @GetMapping("auth/findScheduleList/{hoscode}/{depcode}/{workDate}")
    public R findScheduleList(
            @PathVariable String hoscode,
            @PathVariable String depcode,
            @PathVariable String workDate) {
        List<Schedule> scheduleList = scheduleService.getDetailSchedule(hoscode, depcode, workDate);
        return R.ok().data("scheduleList",scheduleList);
    }

    @ApiOperation(value = "7.获取排班详情-根据排班id")
    @GetMapping("getSchedule/{id}")
    public R getScheduleList(@PathVariable String id) {
        Schedule schedule = scheduleService.getByScheduleId(id);
        return R.ok().data("schedule",schedule);
    }

    @ApiOperation(value = "8.获取预约下单数据-根据排班id")
    @GetMapping("inner/getScheduleOrderVo/{scheduleId}")
    public ScheduleOrderVo getScheduleOrderVo(
            @PathVariable("scheduleId") String scheduleId) {
        return scheduleService.getScheduleOrderVo(scheduleId);
    }
}
