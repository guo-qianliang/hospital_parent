package com.alibaba.yygh.common.exception;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * description: YyghException 自定义异常类
 * 使用时需要在try-catch中手动throws出来
 * @author: gql
 * @date: 2021/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class YyghException extends RuntimeException {

    @ApiModelProperty(value = "状态码")
    private Integer code;

    private String msg;


}
