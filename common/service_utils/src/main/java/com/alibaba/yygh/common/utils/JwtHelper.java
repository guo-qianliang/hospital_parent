package com.alibaba.yygh.common.utils;

import io.jsonwebtoken.*;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * description: JwtHelper
 * @author: gql
 * @date: 2022/3
 */
public class JwtHelper {
    /**
     * token过期时间为1天
     */
    private static long tokenExpiration = 24 * 60 * 60 * 1000;
    /**
     * 加密key
     */
    private static String tokenSignKey = "123456";

    /**
     * 1.生成token字符串
     * @param userId 用户id
     * @param userName 用户姓名
     * @return token
     */
    public static String createToken(Long userId, String userName) {
        String token = Jwts.builder()
                // 设置分类
                .setSubject("YYGH-USER")
                // 设置过期时间
                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
                // 设置id和name
                .claim("userId", userId)
                .claim("userName", userName)
                // 对上面的签名使用HS512进行加密
                .signWith(SignatureAlgorithm.HS512, tokenSignKey)
                // 压缩处理
                .compressWith(CompressionCodecs.GZIP)
                .compact();
        return token;
    }

    /**
     * 2.从token字符串中取出userId
     * @param token token
     * @return userId
     */
    public static Long getUserId(String token) {
        if (StringUtils.isEmpty(token)) {
            return null;
        }
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        Integer userId = (Integer) claims.get("userId");
        return userId.longValue();
    }

    /**
     * 3.从token字符串中取出userName
     * @param token token
     * @return userName
     */
    public static String getUserName(String token) {
        if (StringUtils.isEmpty(token)) {
            return "";
        }
        Jws<Claims> claimsJws
                = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        return (String) claims.get("userName");
    }

    public static void main(String[] args) {
        String token = JwtHelper.createToken(1L, "gql");
        System.out.println(token);
        System.out.println(JwtHelper.getUserId(token));
        System.out.println(JwtHelper.getUserName(token));
    }
}
