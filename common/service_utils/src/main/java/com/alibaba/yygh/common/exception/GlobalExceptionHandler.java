package com.alibaba.yygh.common.exception;

import com.alibaba.yygh.common.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * description: GlobalExceptionHandler 统一异常处理类
 * 常见的异常处理方式有三种：
 * 1.全局异常处理
 * 2.特定异常处理(不常用，因为不能确定报什么异常)
 * 3.自定义异常处理
 * @author: gql
 * @date: 2021/10
 */
//此注解基于AOP,可以理解为我们重写了原先的异常方法
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     * 全局异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    // 返回JSON数据
    @ResponseBody
    public R error(Exception e) {
        e.printStackTrace();
        return R.error().message("全局异常处理提醒:系统发生异常！！");
    }

    /**
     * 自定义异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(YyghException.class)
    // 返回JSON数据
    @ResponseBody
    public R error(YyghException e) {
        log.error("yygh自定义异常:"+e.getMsg());
        e.printStackTrace();
        return R.error().message(e.getMsg()).code(e.getCode());
    }
}
