package com.alibaba.yygh.common.result;

/**
 * description: ResultCode
 * @author: gql
 * @date: 2021/10
 */
public interface ResultCode {
    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 20001;
}
