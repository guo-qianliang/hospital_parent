package com.alibaba.yygh.common.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * description: AuthContextHolder   从请求头token中获取当前用户信息的工具类
 * @author: gql
 * @date: 2021/11
 */


public class AuthContextHolder {
    /**
     * 从请求头token中获取当前用户id
     * @param request
     * @return
     */
    public static Long getUserId(HttpServletRequest request) {
        // 从header获取token
        String token = request.getHeader("token");
        // jwt从token获取userid
        Long userId = JwtHelper.getUserId(token);
        return userId;
    }

    /**
     * 从请求头token中获取当前用户名称
     * @param request
     * @return
     */
    public static String getUserName(HttpServletRequest request) {
        // 从header获取token
        String token = request.getHeader("token");
        // jwt从token获取userid
        String userName = JwtHelper.getUserName(token);
        return userName;
    }
}
