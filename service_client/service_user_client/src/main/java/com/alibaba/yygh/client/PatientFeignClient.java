package com.alibaba.yygh.client;

import com.alibaba.yygh.model.user.Patient;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * description: PatientFeignClient
 * @author: gql
 * @date: 2021/11
 */
@FeignClient("service-user")
//@Repository
public interface PatientFeignClient {

    @ApiOperation(value = "获取就诊人")
    @GetMapping("/api/user/patient/inner/get/{id}")
    Patient getPatient(@PathVariable("id") Long id);
}
