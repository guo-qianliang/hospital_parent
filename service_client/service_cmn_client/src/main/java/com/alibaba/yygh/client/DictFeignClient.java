package com.alibaba.yygh.client;

import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * description: DictFeignClient
 * @author: gql
 * @date: 2021/11
 */
@FeignClient("service-cmn")
public interface DictFeignClient {
    /**
     * 4.根据dictCode和value获取数据字典名称
     * @param dictCode
     * @param value
     * @return
     */
    @GetMapping(value = "/admin/cmn/dict/getName/{dictCode}/{value}")
    public String getName(
            @PathVariable("dictCode") String dictCode,
            @PathVariable("value") String value);

    /**
     * 5.根据value获取数据字典名称
     * @param value
     * @return
     */
    @GetMapping(value = "/admin/cmn/dict/getName/{value}")
    public String getName(
            @PathVariable("value") String value);
}
